package com.dawn.system.domain.dto;

import com.dawn.common.base.BaseEntity;
import lombok.Data;

import java.util.List;

/**
 * <p>script-control-com.dawn.system.domain.dto-UserRoleDTO</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Data
public class UserRoleDTO extends BaseEntity {
    private static final long serialVersionUID = -690585535450183088L;
    private Long userId;
    private String loginName;
    private List<RolePermissionDTO> roles;


}
