package com.dawn.system.domain.vo;

import com.dawn.common.base.BaseEntity;
import lombok.Data;

/**
 * <p>script-control-com.dawn.system.domain.vo-PermissionVO</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Data
public class PermissionVO extends BaseEntity {
    private static final long serialVersionUID = -8951277644320476453L;

    private Long permissionId;

    private String permissionName;

    private String permissionType;


}
