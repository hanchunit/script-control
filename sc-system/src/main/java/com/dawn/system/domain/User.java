package com.dawn.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.dawn.common.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>script-control-com.dawn.system.domain-User</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Data
@TableName("sys_user")
@ApiModel(value = "用户实体")
public class User extends BaseEntity {
    private static final long serialVersionUID = -1405654983557425826L;
    /**
     * 用户标识
     */
    @ApiModelProperty(value = "用户标识")
    private Long userId;
    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名")
    private String userName;
    /**
     * 登录名
     */
    @ApiModelProperty(value = "登录名")
    private String loginName;
    /**
     * 登录密码
     */
    @ApiModelProperty(value = "登录密码")
    private String loginPassword;
    /**
     * 用户类型
     */
    @ApiModelProperty(value = "用户类型")
    private Integer userType;
    /**
     * 用户状态
     */
    @ApiModelProperty(value = "用户状态")
    private Integer userState;
    @ApiModelProperty(value = "用户token")
    private String userToken;
    @ApiModelProperty(value = "登录时间")
    private Date loginTime;
    @ApiModelProperty(value = "用户设备IMEI")
    private String userDeviceId;

    @JsonValue
    public Map<String, Object> toMap() {
        Map<String, Object> userMap = new HashMap<>();
        userMap.put("userName", getUserName());
        userMap.put("loginName", getLoginName());
        userMap.put("userType", getUserType());
        userMap.put("userState", getUserState());
        userMap.put("userDeviceId", getUserDeviceId());

        return userMap;
    }


}
