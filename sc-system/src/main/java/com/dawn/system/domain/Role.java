package com.dawn.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.dawn.common.base.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * <p>script-control-com.dawn.system.domain-Role</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Data
@TableName("sys_role")
public class Role extends BaseEntity {
    private static final long serialVersionUID = 4265459723344374542L;


    private Long roleId;

    private String roleName;

    private String status;

    private String delFlag;

    private String createBy;

    private Date createTime;

    private String updateBy;

    private Date updateTime;

    private String remark;


}
