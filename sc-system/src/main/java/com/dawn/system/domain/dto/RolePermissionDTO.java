package com.dawn.system.domain.dto;

import com.dawn.common.base.BaseEntity;
import com.dawn.system.domain.vo.PermissionVO;
import lombok.Data;

import java.util.List;

/**
 * <p>script-control-com.dawn.system.domain.dto-RolePermissionDTO</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Data
public class RolePermissionDTO extends BaseEntity {
    private static final long serialVersionUID = 4354046052851286172L;
    private Long roleId;

    private String roleName;

    private List<PermissionVO> permissions;


}
