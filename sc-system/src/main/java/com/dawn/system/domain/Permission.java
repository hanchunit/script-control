package com.dawn.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.dawn.common.base.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * <p>script-control-com.dawn.system.domain-PermissionVO</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */
@Data
@TableName("sys_permission")

public class Permission extends BaseEntity {
    private static final long serialVersionUID = 4929093043543938250L;
    private Long permissionId;

    private String permissionName;

    private String permissionType;

    private String delFlag;

    private String createBy;

    private Date createTime;

    private String updateBy;

    private Date updateTime;

    private String remark;
}
