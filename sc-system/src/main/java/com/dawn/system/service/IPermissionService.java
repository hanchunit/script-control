package com.dawn.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dawn.system.domain.Permission;

/**
 * <p>script-control-com.dawn.system.service-IUserService</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

public interface IPermissionService extends IService<Permission> {

}
