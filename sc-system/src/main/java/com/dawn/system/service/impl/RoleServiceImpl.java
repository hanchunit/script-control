package com.dawn.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dawn.system.domain.Role;
import com.dawn.system.mapper.RoleMapper;
import com.dawn.system.service.IRoleService;
import org.springframework.stereotype.Service;

/**
 * <p>script-control-com.dawn.system.service.impl-UserServiceImpl</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {

}
