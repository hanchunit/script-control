package com.dawn.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dawn.system.domain.Permission;
import com.dawn.system.mapper.PermissionMapper;
import com.dawn.system.service.IPermissionService;
import org.springframework.stereotype.Service;

/**
 * <p>script-control-com.dawn.system.service.impl-UserServiceImpl</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements IPermissionService {

}
