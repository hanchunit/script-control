package com.dawn.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dawn.common.result.ResultData;
import com.dawn.system.domain.User;
import com.dawn.system.domain.dto.UserPermissionDTO;
import com.dawn.system.domain.dto.UserRoleDTO;

import java.io.Serializable;

/**
 * <p>script-control-com.dawn.system.service-IUserService</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

public interface IUserService extends IService<User> {
    /**
     * 搜索符合条件的用户返回
     *
     * @param user 条件
     * @return 用户集合
     */
    public ResultData selectListByUser(User user);

    /**
     * 用户登录
     *
     * @param loginName     登录名
     * @param loginPassword 登录密码
     * @return token|null
     */
    public Boolean login(String loginName, String loginPassword);

    /**
     * 验证是否有此用户名
     *
     * @param loginName 用户登录名
     * @return true|false
     */
    public Boolean isUserByLoginName(String loginName);

    /**
     * 设置用户的token
     *
     * @param username 用户实体
     * @return true|false
     */
    public String setToken(String username);

    /**
     * 根据用户id获取用户角色
     *
     * @param userId 用户id
     * @return 用户角色
     */
    public UserRoleDTO getUserRoleById(Serializable userId);

    /**
     * 根据用户id获取用户权限
     *
     * @param userId 用户id
     * @return 用户权限
     */
    public UserPermissionDTO getUserPermissionById(Serializable userId);

    /**
     * 根据用户token获取用户权限
     *
     * @param userToken 用户token
     * @return 用户权限
     */
    public UserPermissionDTO getUserPermissionByToken(String userToken);

    /**
     * 根据用户token获取用户id
     *
     * @param userToken 用户token
     * @return 用户id
     */
    public Long getUserIdByToken(String userToken);

    /**
     * 根据用户登录名获取用户状态
     *
     * @param loginName 用户登录名
     * @return 用户状态
     */
    public Integer selectUserStateByLoginName(String loginName);

    /**
     * 根据用户设备IMEI获取用户状态
     *
     * @param userDeviceId 设备IMEI
     * @return 用户状态
     */
    public Integer selectUserStateByUserDeviceId(String userDeviceId);

    /**
     * 根据用户token获取用户类型
     *
     * @param userToken 用户token
     * @return 用户类型
     */
    public Integer getUserTypeByToken(String userToken);

    /**
     * 根据用户token获取用户状态
     *
     * @param userToken 用户token
     * @return 用户状态
     */
    public Integer getUserStateByToken(String userToken);

    /**
     * 根据用户token获取用户id
     *
     * @param deviceId 设备IMEI
     * @return 用户id
     */
    public String getLoginNameByDeviceId(String deviceId);

    /**
     * 验证用户根据用户token
     *
     * @param userToken 用户token
     * @return true|false
     */
    public Boolean isUserByToken(String userToken);

    /**
     * 验证设备是否激活
     *
     * @param userDeviceId 设备IMEI
     * @return true|false
     */
    public Boolean isUserDeviceId(String userDeviceId);

    /**
     * 激活设备
     *
     * @param user 用户实体
     * @return true|false
     */
    public Boolean activateDevice(User user);

    /**
     * 取消激活设备
     *
     * @param user 用户实体
     * @return true|false
     */
    public Boolean deactivateDevice(User user);


}
