package com.dawn.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dawn.common.exception.user.NotUsernameOrPasswordException;
import com.dawn.common.result.ResultData;
import com.dawn.common.util.TokenTool;
import com.dawn.system.domain.User;
import com.dawn.system.domain.dto.UserPermissionDTO;
import com.dawn.system.domain.dto.UserRoleDTO;
import com.dawn.system.mapper.UserMapper;
import com.dawn.system.service.IUserService;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * <p>script-control-com.dawn.system.service.impl-UserServiceImpl</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {


    @Override
    public ResultData selectListByUser(User user) {
        return new ResultData(getBaseMapper().selectListByUser(user));
    }

    @Override
    public Boolean login(String loginName, String loginPassword) {
        User user = new User();
        user.setLoginName(loginName);
        user.setLoginPassword(loginPassword);
        if (!getBaseMapper().login(user)) {
            throw new NotUsernameOrPasswordException();
        } else {
            return true;
        }
    }

    @Override
    public Boolean isUserByLoginName(String loginName) {
        return getBaseMapper().isUserByLoginName(loginName);
    }

    @Override
    public String setToken(String username) {
        User user = new User();
        user.setUserName(username);
        String token = TokenTool.createToken();
        user.setUserToken(token);
        return getBaseMapper().setToken(user) ? token : null;
    }

    @Override
    public UserRoleDTO getUserRoleById(Serializable userId) {
        return getBaseMapper().getUserRoleById(userId);
    }

    @Override
    public UserPermissionDTO getUserPermissionById(Serializable userId) {
        return getBaseMapper().getUserPermissionById(userId);
    }

    @Override
    public UserPermissionDTO getUserPermissionByToken(String userToken) {
        return getBaseMapper().getUserPermissionByToken(userToken);
    }

    @Override
    public Long getUserIdByToken(String userToken) {
        return getBaseMapper().getUserIdByToken(userToken);
    }

    @Override
    public Integer getUserTypeByToken(String userToken) {
        return getBaseMapper().getUserTypeByToken(userToken);
    }

    @Override
    public Integer selectUserStateByLoginName(String loginName) {
        return getBaseMapper().selectUserStateByLoginName(loginName);
    }

    @Override
    public Integer selectUserStateByUserDeviceId(String userDeviceId) {
        return getBaseMapper().selectUserStateByUserDeviceId(userDeviceId);
    }

    @Override
    public Integer getUserStateByToken(String userToken) {
        return getBaseMapper().getUserStateByToken(userToken);
    }

    @Override
    public Boolean isUserByToken(String userToken) {
        return getBaseMapper().isUserByToken(userToken);
    }

    /**
     * 验证设备是否激活
     *
     * @param userDeviceId 设备IMEI
     * @return true|false
     */
    @Override
    public Boolean isUserDeviceId(String userDeviceId) {
        return getBaseMapper().isUserDeviceId(userDeviceId);
    }

    @Override
    public String getLoginNameByDeviceId(String deviceId) {
        return getBaseMapper().getLoginNameByDeviceId(deviceId);
    }

    /**
     * 激活设备
     *
     * @param user 用户实体
     * @return true|false
     */
    @Override
    public Boolean activateDevice(User user) {
        return getBaseMapper().activateDevice(user);
    }

    /**
     * 取消激活设备
     *
     * @param user 用户实体
     * @return true|false
     */
    @Override
    public Boolean deactivateDevice(User user) {
        return getBaseMapper().deactivateDevice(user);
    }
}
