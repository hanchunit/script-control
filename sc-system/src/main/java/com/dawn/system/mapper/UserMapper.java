package com.dawn.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dawn.system.domain.User;
import com.dawn.system.domain.dto.UserPermissionDTO;
import com.dawn.system.domain.dto.UserRoleDTO;
import org.apache.ibatis.annotations.Mapper;

import java.io.Serializable;
import java.util.List;

/**
 * <p>script-control-com.dawn.system.mapper-UserMapper</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
    /**
     * 根据用户实体查询符合条件的用户
     *
     * @param user 用户实体
     * @return 用户集合
     */
    public List<User> selectListByUser(User user);

    /**
     * 验证登录名是否存在
     *
     * @param loginName 登录名
     * @return true|false
     */
    public Boolean isUserByLoginName(String loginName);

    /**
     * 登录验证登录名和密码是否匹配
     *
     * @param user 用户实体
     * @return true|false
     */
    public Boolean login(User user);

    /**
     * 设置用户的token
     *
     * @param user 用户实体
     * @return true|false
     */
    public Boolean setToken(User user);

    /**
     * 获取用户角色
     *
     * @param userId 用户id
     * @return 用户角色
     */
    public UserRoleDTO getUserRoleById(Serializable userId);

    /**
     * 获取用户权限
     *
     * @param userId 用户id
     * @return 用户权限
     */
    public UserPermissionDTO getUserPermissionById(Serializable userId);

    /**
     * 获取用户权限
     *
     * @param userToken 用户token
     * @return 用户权限
     */
    public UserPermissionDTO getUserPermissionByToken(String userToken);

    /**
     * 获取用户id
     *
     * @param userToken 用户token
     * @return 用户id
     */
    public Long getUserIdByToken(String userToken);

    /**
     * 获取用户状态
     *
     * @param loginName 登录名
     * @return 用户状态
     */
    public Integer selectUserStateByLoginName(String loginName);

    /**
     * 获取用户状态
     *
     * @param userDeviceId 设备IMEI
     * @return 用户状态
     */
    public Integer selectUserStateByUserDeviceId(String userDeviceId);

    /**
     * 获取用户类型
     *
     * @param userToken 用户token
     * @return 用户类型
     */
    public Integer getUserTypeByToken(String userToken);

    /**
     * 获取用户状态
     *
     * @param userToken 用户token
     * @return 用户状态
     */
    public Integer getUserStateByToken(String userToken);

    /**
     * 验证token是否存在
     *
     * @param userToken 用户token
     * @return true|false
     */
    public Boolean isUserByToken(String userToken);

    /**
     * 验证设备是否激活
     *
     * @param userDeviceId 设备IMEI
     * @return true|false
     */
    public Boolean isUserDeviceId(String userDeviceId);

    /**
     * 获取用户id
     *
     * @param  userDeviceId 设备IMEI
     * @return 用户id
     */
    public String getLoginNameByDeviceId(String userDeviceId);

    /**
     * 激活设备
     *
     * @param user 用户实体
     * @return true|false
     */
    public Boolean activateDevice(User user);

    /**
     * 取消激活设备
     *
     * @param user 用户实体
     * @return true|false
     */
    public Boolean deactivateDevice(User user);
}
