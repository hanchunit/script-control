package com.dawn.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dawn.system.domain.Permission;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>script-control-com.dawn.system.mapper-UserMapper</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */
@Mapper
public interface PermissionMapper extends BaseMapper<Permission> {


}
