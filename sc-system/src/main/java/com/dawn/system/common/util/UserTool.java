package com.dawn.system.common.util;

import com.dawn.common.constant.user.UserConstant;
import com.dawn.common.exception.user.*;
import com.dawn.common.exception.user.device.DeviceException;
import com.dawn.common.exception.user.device.NotActivateException;
import com.dawn.common.util.string.StringTool;
import com.dawn.system.domain.User;
import com.dawn.system.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * <p>script-control-com.dawn.system.common.util-UserTool</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Component
public class UserTool {

    private static UserTool userTool;
    @Autowired
    private IUserService userService;

    /**
     * 验证登录
     *
     * @param token token令牌
     * @return true|false
     */
    public static Boolean isLogin(String token) {
        //判断token是否为空
        if (StringTool.isNull(token)) {
            throw new NotLoginException();
        }
        //判断token是否有效
        if (userTool.userService.isUserByToken(token)) {
            throw new NotTokenException();
        }
        Integer userState = userTool.userService.getUserStateByToken(token);
        return isUserState(userState);
    }

    /**
     * 验证用户状态是否可用
     *
     * @param state 用户状态
     * @return true|false
     */
    public static Boolean isUserState(Integer state) {
        if (state == null) {
            throw new UserStateException();
        } else if (state == UserConstant.USER_STATE_NORMAL) {
            return true;
        } else if (state == UserConstant.USER_STATE_LOCK) {
            throw new UserLockException();
        } else if (state == UserConstant.USER_STATE_DISABLE) {
            throw new UserDisableException();
        }
        throw new UserStateException();
    }

    /**
     * 用户登录
     *
     * @return 用户token
     */
    public static String login(String username, String password) {
        if (StringTool.isNull(username) || StringTool.isNull(password)) {
            throw new NotUsernameOrPasswordException();
        }
        String token;

        //验证用户名
        if (!userTool.userService.isUserByLoginName(username)) {
            throw new NotUsernameException();
            //验证密码
        } else if (!userTool.userService.login(username, password)) {
            throw new NotUsernameOrPasswordException();
        }//设置token令牌
        token = userTool.userService.setToken(username);
        if (StringTool.isNull(token)) {
            throw new SetTokenException();
        } else {
            return token;
        }
    }

    /**
     * 验证设备是否激活
     *
     * @param deviceId 设备IMEI
     * @return true|false
     */
    public static String isActivate(String deviceId) {
        if (StringTool.isNull(deviceId)) {
            throw new DeviceException();
        } else if (!userTool.userService.isUserDeviceId(deviceId)) {
            throw new NotActivateException();
        }
        Integer userState = userTool.userService.selectUserStateByUserDeviceId(deviceId);
        return isUserState(userState) ? userTool.userService.getLoginNameByDeviceId(deviceId) : null;
    }

    /**
     * 激活设备
     *
     * @param username 用户名
     * @param password 密码
     * @param deviceId 设备IMEI
     * @return true｜false
     */
    public static Boolean activateDevice(String username, String password, String deviceId) {
        if (userTool.userService.isUserDeviceId(deviceId)) {
            return true;
        } else if (!userTool.userService.isUserByLoginName(username)) {
            throw new NotUsernameException();
        } else if (!userTool.userService.login(username, password)) {
            throw new NotUsernameOrPasswordException();
        }
        User user = new User();
        user.setLoginName(username);
        user.setUserDeviceId(deviceId);
        return userTool.userService.activateDevice(user);
    }

    /**
     * 取消激活设备
     *
     * @param deviceId 设备IMEI
     * @return true｜false
     */
    public static Boolean deactivateDevice(String deviceId) {
        User user = new User();
        user.setUserDeviceId(deviceId);
        if (!userTool.userService.isUserDeviceId(deviceId)) {
            throw new NotActivateException();
        } else {
            return userTool.userService.deactivateDevice(user);

        }


    }

    @PostConstruct
    public void init() {
        userTool = this;
        userTool.userService = this.userService;
    }
}