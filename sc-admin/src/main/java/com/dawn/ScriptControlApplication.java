package com.dawn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>script-control-com.dawn-ScriptControlApplication</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@SpringBootApplication
public class ScriptControlApplication {
    public static void main(String[] args) {
        SpringApplication.run(ScriptControlApplication.class, args);
    }


}
