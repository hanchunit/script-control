package com.dawn.config.system;

import com.dawn.common.annotation.TokenAnnotation;
import com.dawn.common.exception.user.NotLoginException;
import com.dawn.common.exception.user.NotPermissionException;
import com.dawn.common.exception.user.NotTokenException;
import com.dawn.system.domain.dto.UserPermissionDTO;
import com.dawn.system.domain.vo.PermissionVO;
import com.dawn.system.service.IUserService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>script-control-com.dawn.system.config-TokenAop</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */
@Component
@Order(2)
@Aspect
public class TokenAop {

    @Autowired
    private IUserService userService;

    /**
     * 定义切点
     */
    @Pointcut("@annotation(com.dawn.common.annotation.TokenAnnotation)")
    public void tokenAop() {
    }

    /**
     * 权限环绕通知
     *
     * @param
     */
    @Before("tokenAop()")
    @ResponseBody
    public void isAccessMethod(JoinPoint point) {
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        ServletRequestAttributes sra = (ServletRequestAttributes) ra;
        HttpServletRequest request = sra.getRequest();
        //    throw  new NotTokenException();
        // System.out.println("--环绕通知开始--");
        MethodSignature signature = (MethodSignature) point.getSignature();
        TokenAnnotation tokenAnnotation = signature.getMethod().getDeclaredAnnotation(TokenAnnotation.class);

        String permissionName = tokenAnnotation.permissionName();
        String permissionType = tokenAnnotation.permissionType();

        String token = request.getHeader("token");
        //  System.out.println("***************");
        System.out.println(token);
        if (token == null || "".equals(token)) {
            throw new NotLoginException();
        }

        if (!userService.isUserByToken(token)) {
            throw new NotTokenException();
        }

        if (userService.getUserTypeByToken(token) == 0) {
            return;
        }
        //获取当前用户的权限
        UserPermissionDTO userPermission = userService.getUserPermissionByToken(token);
        if (userPermission != null) {
            for (PermissionVO permission : userPermission.getPermissions()) {
                if (permission.getPermissionName().equals(permissionName) && permission.getPermissionType().equals(permissionType)) {
                    return;
                }
            }
        }
        throw new NotPermissionException();
    }

    /**
     * 获取Request请求
     */
    private HttpServletRequest getRequest() {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        return attributes.getRequest();
    }

}





