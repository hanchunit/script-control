package com.dawn.config.system;

import com.dawn.common.annotation.ActivateDeviceAnnotation;
import com.dawn.common.exception.user.device.NotActivateException;
import com.dawn.common.util.string.StringTool;
import com.dawn.system.common.util.UserTool;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>script-control-com.dawn.system.config-ActivateDeviceAop</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


@Component
@Order(1)
@Aspect
public class ActivateDeviceAop {

    /**
     * 定义切点
     */
    @Pointcut("@annotation(com.dawn.common.annotation.ActivateDeviceAnnotation)")
    public void activateDeviceAop() {
    }

    /**
     * 权限环绕通知
     *
     * @param point 切点
     */
    @Before("activateDeviceAop()")
    @ResponseBody
    public void isAccessMethod(JoinPoint point) {
        System.out.println("是否登录aop");
        MethodSignature signature = (MethodSignature) point.getSignature();
        ActivateDeviceAnnotation activateDeviceAnnotation = signature.getMethod().getDeclaredAnnotation(ActivateDeviceAnnotation.class);
        HttpServletRequest request = getRequest();

        boolean isActivate = activateDeviceAnnotation.isActivate();
        if (isActivate) {
            String deviceId = request.getHeader("deviceId");
            //如果没有token或者token对应的用户为0则用户未登录
            boolean isActivates = !StringTool.isNull(UserTool.isActivate(deviceId));
            if (!isActivates) {
                throw new NotActivateException();
            }
        }

    }

    /**
     * 获取Request请求
     */
    private HttpServletRequest getRequest() {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();

        return attributes.getRequest();
    }
}
