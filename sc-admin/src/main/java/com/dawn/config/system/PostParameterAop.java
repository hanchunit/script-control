package com.dawn.config.system;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * <p>script-control-com.dawn.system.config-RestControllerParameterAop</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Component
@Order(99)
@Aspect
public class PostParameterAop {

    /**
     * 定义切点
     */
  //  @Pointcut("@annotation(org.springframework.web.bind.annotation.PostMapping)")
    public void postParameterAop() {
    }

    //@Before(value = "postParameterAop()")
    public void before(JoinPoint point) {
        String methodName = point.getSignature().getName();
        List<Object> args = Arrays.asList(point.getArgs());
        String[] argNames = ((MethodSignature) point.getSignature()).getParameterNames();
        HttpServletRequest request = getRequest();
        Map<String, Object> jsonParam = this.getJSONParam(getRequest());
//        for (String argName : argNames) {
//
//        }
        System.out.println("******************前置通知开始********************");
        System.out.println("******************前置通知开始********************");
        System.out.println("******************前置通知开始********************");
        System.out.println(Arrays.toString(argNames));
        System.out.println(methodName);
        System.out.println(args);
        System.out.println(jsonParam);
        System.out.println("******************前置通知结束********************");
        System.out.println("******************前置通知结束********************");
        System.out.println("******************前置通知结束********************");
    }

    /**
     * @param joinPoint
     * @return
     * @throws Throwable
     */
    //  @Around("postParameterAop()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {

        //获取目标方法参数
        Object[] args = joinPoint.getArgs();
        System.out.println("******************方法参数开始********************");
        System.out.println(Arrays.toString(args));
        System.out.println("******************方法参数结束********************");
        //方法参数名称
        String[] argName = ((MethodSignature) joinPoint.getSignature()).getParameterNames();
        //   JSONObject jsonObject = this.getJSONParam(getRequest());

        Map<String, Object> jsonObject = this.getJSONParam(getRequest());
        Map<String, Object> param = this.getJSONParam(getRequest());
        System.out.println("****************json参数开始**********************");
        System.out.println(jsonObject);
        System.out.println("*********************json参数结束*****************");

        if (jsonObject == null || argName.length == 0) {
            return joinPoint.proceed();
        }
        //json参数

        for (int i = 0; i < argName.length; i++) {
            if (param.get(argName[i]) != null && !"".equals(param.get(argName[i]))) {
                args[i] = param.get(argName[i]);
            }
        }
        //执行目标方法
        return joinPoint.proceed(args);

    }

    /**
     * 获取Request请求
     */
    private HttpServletRequest getRequest() {
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        ServletRequestAttributes sra = (ServletRequestAttributes) ra;

        return sra.getRequest();
    }

    /**
     * 获取接收到的json参数
     *
     * @param request 请求
     * @return json
     */
    public Map<String, Object> getJSONParam(HttpServletRequest request) {
        ObjectMapper objectMapper = new ObjectMapper();
        String param = null;
        try {

            BufferedReader streamReader = new BufferedReader(new InputStreamReader(request.getInputStream(), StandardCharsets.UTF_8));

            StringBuilder responseStrBuilder = new StringBuilder();
            String inputStr;
            while ((inputStr = streamReader.readLine()) != null) {
                responseStrBuilder.append(inputStr);
            }
            Map<String, Object> jsonMap = null;// objectMapper.readValue(streamReader.readLine(), Map.class);

            param = "";//jsonMap.toString();
            System.out.println("****************json参数开始1**********************");
            System.out.println(streamReader);
            System.out.println(param);
            System.out.println("*********************json参数结束1*****************");
            return jsonMap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
