package com.dawn.config.admin;


import com.dawn.common.exception.base.BaseException;
import com.dawn.common.result.Result;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * <p>script-control-com.dawn.config-easeear</p>
 * <p>Description:  全局异常捕获类(作用是异常信息统一格式输出) </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */
@Component
@RestControllerAdvice

public class MyResponseBodyAdvice {

    public MyResponseBodyAdvice() {
    }

//    /**
//     * 全局异常Exception捕获类
//     *
//     * @param exception 异常
//     * @return 返回
//     */
//    @ExceptionHandler({Exception.class})
//    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
//    public Result<String> uniteExceptionHandler(BaseException exception) {
//        return Result.exception(exception);
//        //  return Result.error("系统异常");
//    }

    /**
     * 全局自定义异常TestException捕获类
     *
     * @param baseException
     * @return
     */
    @ExceptionHandler(value = BaseException.class)
    public Result<String> userExceptionHandler(BaseException baseException) {

        return Result.exception(baseException);
    }


}