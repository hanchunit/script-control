package com.dawn.config.admin;

import com.dawn.common.exception.user.NotLoginOrNotActivateException;
import com.dawn.common.util.string.StringTool;
import com.dawn.system.common.util.UserTool;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p>script-control-com.dawn.config-FileHandlerInterceptor</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */
@Component
//@Configuration
public class FileHandlerInterceptor implements HandlerInterceptor {


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws ServletException, IOException {
        System.out.println("**************静态资源权限校验*****************");
        String token = request.getHeader("token");
        String deviceId = request.getHeader("deviceId");
        try {

            if (StringTool.isNull(token) && StringTool.isNull(deviceId)) {
                throw new NotLoginOrNotActivateException();
            } else if (!StringTool.isNull(token) && StringTool.isNull(deviceId)) {
                return UserTool.isLogin(token);
            } else if (StringTool.isNull(token) && !StringTool.isNull(deviceId)) {
                return !StringTool.isNull(UserTool.isActivate(deviceId));
            } else if (!StringTool.isNull(token) && !StringTool.isNull(deviceId)) {
                return UserTool.isLogin(token) && !StringTool.isNull(UserTool.isActivate(deviceId));
            }
        } catch (Exception e) {
            request.setAttribute("filter.error", e);
            //将异常分发到/baseException/errors控制器
            request.getRequestDispatcher("/baseException/errors").forward(request, response);
        }

        return false;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }

}
