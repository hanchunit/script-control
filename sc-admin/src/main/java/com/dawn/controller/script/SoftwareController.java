package com.dawn.controller.script;

import com.dawn.common.config.MorningBreezeSoftwareConfig;
import com.dawn.common.result.Result;
import com.dawn.common.util.file.FileTool;
import com.dawn.common.util.string.StringTool;
import com.dawn.script.domain.Software;
import com.dawn.script.service.ISoftwareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * <p>script-control-com.dawn.controller.script-SoftwareController</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@RestController
@RequestMapping("/software")
public class SoftwareController {
    @Autowired
    ISoftwareService softwareService;


    /**
     * 获取软件更新
     *
     * @return 结果
     */
    @GetMapping("/updateSoftware")
    public Software updateVersion(String version) {
        Software software = softwareService.newestSoftware();
        if (version.equals(software.getVersion())) {
            return null;
        }
        String iecFile = software.getDownloadUrl();
        String apkFile = software.getSoftwareUrl();
        software.setSoftwareUrl("https://t4060916n0.goho.co" + MorningBreezeSoftwareConfig.getSrcPath() + apkFile);
        software.setDownloadUrl("https://t4060916n0.goho.co" + MorningBreezeSoftwareConfig.getSrcPath() + iecFile);
        return software;
    }


    /**
     * 提交软件更新
     *
     * @return 结果
     */
    @PostMapping("/saveSoftware")
    public Result<String> saveSoftware(String version, Boolean dialog, String msg, Boolean force, @RequestParam("apkFile") MultipartFile apkFile, @RequestParam("iecFile") MultipartFile iecFile) throws IOException {
       String apkUrl = FileTool.fileUpload("apk\\", "autoScript" + version, ".apk", apkFile);
        String iecUrl = FileTool.fileUpload("iec\\", "autoScript" + version, ".iec", iecFile);


        Software software = new Software();
        if (!StringTool.isNull(apkUrl) && !StringTool.isNull(iecUrl)) {
            software.setSoftwareUrl("/apk/" + "autoScript" + version + ".apk");
            software.setDownloadUrl("/iec/" + "autoScript" + version + ".iec");
            software.setVersion(version);
            software.setDialog(dialog);
            software.setMsg(msg);
            software.setForce(force);
        }

        return Result.okOrFailed(softwareService.saveOrUpdateSoftware(software), "保存成功", "保存失败");
    }


}
