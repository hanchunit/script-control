package com.dawn.controller.script;

import com.dawn.arknight.service.IInstanceSiteService;
import com.dawn.common.annotation.ActivateDeviceAnnotation;
import com.dawn.common.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>script-control-com.dawn.script.controller-ResourcesController</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@RestController
@RequestMapping("/resources")
public class ResourcesController {

    @Autowired
    IInstanceSiteService instanceSiteService;

    @ActivateDeviceAnnotation
    @PostMapping("/get_instanceSiteImage")
    public Result<List<String>> instanceSiteImage() {
        return Result.ok(instanceSiteService.siteListByType(1));
    }

    @ActivateDeviceAnnotation
    @PostMapping("/get_instanceSiteImages")
    public Result<List<String>> instanceSiteImages() {
        return Result.ok(instanceSiteService.siteImages());
    }

    @ActivateDeviceAnnotation
    @PostMapping("/get_instanceSiteColor")
    public Result<List<String>> instanceSiteColor() {
        return Result.ok(instanceSiteService.siteListByType(0));
    }

}
