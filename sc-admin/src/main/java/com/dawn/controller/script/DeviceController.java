package com.dawn.controller.script;

import com.dawn.common.result.Result;
import com.dawn.common.util.string.StringTool;
import com.dawn.system.common.util.UserTool;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>script-control-com.dawn.script.controller-DeviceController</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */
@RestController
@RequestMapping("/device")
public class DeviceController {


    /**
     * 激活设备
     *
     * @param username 用户名
     * @param password 密码
     * @return 结果
     */
    @PostMapping("/activateDevice")
    public Result<String> activateDevice(HttpServletRequest request, String username, String password) {
        String deviceId = request.getHeader("deviceId");
        //  System.out.println(deviceId);
        return Result.okOrFailed(UserTool.activateDevice(username, password, deviceId), "设备激活成功", "设备激活失败");
    }

    /**
     * 验证设备激活状态
     *
     * @return 结果
     */
    @PostMapping("/isActivateDevice")
    public Result<String> isActivateDevice(HttpServletRequest request) {
        String deviceId = request.getHeader("deviceId");
        System.out.println(deviceId);
        String data = UserTool.isActivate(deviceId);
        return StringTool.isNull(data) ? Result.failed("设备未激活", data) : Result.ok("设备已激活", data);
    }

    /**
     * 取消设备激活状态
     *
     * @return 结果
     */
    @PostMapping("/deactivateDevice")
    public Result<String> deactivateDevice(HttpServletRequest request) {
        String deviceId = request.getHeader("deviceId");
        return Result.okOrFailed(UserTool.deactivateDevice(deviceId), "取消激活成功", "取消激活失败");
    }

}
