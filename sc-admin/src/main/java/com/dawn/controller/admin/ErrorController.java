package com.dawn.controller.admin;

import com.dawn.common.exception.system.SystemException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>script-control-com.dawn.controller-ErrorController</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */
@RestController
@RequestMapping("/baseException")
public class ErrorController {


    @RequestMapping("/errors")
    public void error(HttpServletRequest request) throws Exception {
        Exception error = (Exception) request.getAttribute("filter.error");
        if (error == null) {
            throw new SystemException();
        } else {
            throw error;

        }
    }
}
