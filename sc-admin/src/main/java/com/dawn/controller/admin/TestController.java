package com.dawn.controller.admin;

import com.dawn.common.result.Result;
import com.dawn.common.util.file.FileTool;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>script-control-com.dawn.controller-TestController</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@RestController
@RequestMapping("/test")
public class TestController {


    @PostMapping("/filePath")
    public Result<Object> filePathTest(@RequestBody Map<String, Object> map) {
        Map<String, Object> maps = new HashMap<>();
        List<String> list1 = new ArrayList<>();
        list1.add("/2-1-1.png");
        list1.add("/2-1-2.png");

        List<String> list2 = new ArrayList<>();
        list2.add("/2-2-1.png");
        list2.add("/2-2-2.png");

        Map<String, Object> map1 = new HashMap<>();
        map1.put("url2-1", list1);
        map1.put("url2-2", list2);

        maps.put("url1-1", map1);
        System.out.println("maps:" + FileTool.joinUrl(maps, "/file"));
        System.out.println("map1:" + FileTool.joinUrl(map1, "/file/url1-1"));
        System.out.println("list1:" + FileTool.joinUrl(list1, "/file/url1-1/url2-1"));
        System.out.println("file:" + FileTool.joinUrl("/2-1-1.png", "/file/url1-1/url2-1"));
        return Result.ok(FileTool.joinUrl(maps, "/file"));
    }


}
