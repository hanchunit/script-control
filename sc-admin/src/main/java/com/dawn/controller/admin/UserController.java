package com.dawn.controller.admin;

import com.dawn.common.base.BaseController;
import com.dawn.common.result.Result;
import com.dawn.domain.vo.UserLoginVO;
import com.dawn.system.common.util.UserTool;
import com.dawn.system.domain.dto.UserPermissionDTO;
import com.dawn.system.domain.dto.UserRoleDTO;
import com.dawn.system.service.IUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

import static com.dawn.common.util.string.StringTool.isNull;

/**
 * <p>script-control-com.dawn.controller-UserController</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */
@Api(tags = "用户模块")
@RestController
@RequestMapping("/user")
public class UserController extends BaseController {
    @Autowired
    private IUserService userService;

    @ApiOperation(value = "登录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "用户名", required = true, dataTypeClass = String.class, paramType = "body"),
            @ApiImplicitParam(name = "password", value = "密码", required = true, dataTypeClass = String.class, paramType = "body")
    })
    @PostMapping("/login")
    public Result<Map<String, Object>> login(@RequestParam(value = "username", required = false) String username, @RequestParam(value = "password", required = false) String password, @RequestBody(required = false) UserLoginVO userLoginVO) {
        if (isNull(username)) {
            username = userLoginVO.getUsername();
        }
        if (isNull(password)) {
            password = userLoginVO.getPassword();
        }

        String token = UserTool.login(username, password);
        if (isNull(token)) {
            return Result.failed("登录失败");
        }
        Map<String, Object> map = new HashMap<>();
        map.put("username", username);
        map.put("token", token);
        return Result.ok(map);
    }

    @GetMapping("getRoleById/{id}")
    public Result<UserRoleDTO> getUserRoleById(@PathVariable String id) {
        return Result.ok(userService.getUserRoleById(id));
    }

    @GetMapping("getPermissionById/{id}")
    public Result<UserPermissionDTO> getUserPermissionById(@PathVariable String id) {
        return Result.ok(userService.getUserPermissionById(id));
    }


}
