package com.dawn.controller.admin;

import com.dawn.common.util.file.image.ImageTool;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>script-control-com.dawn.controller-ImageTest</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */
@RestController
@RequestMapping("/image")
public class ImageTest {

    @PostMapping("/save_photo")
    public String addDish(@RequestParam("photos") MultipartFile file, HttpServletRequest request) {
        return ImageTool.imageUpload(file, "user", "name");
    }
}


