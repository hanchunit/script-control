package com.dawn.controller.arknight;

import com.dawn.arknight.domain.dto.JobSaveDTO;
import com.dawn.arknight.domain.vo.JobMaterial;
import com.dawn.arknight.service.IJobService;
import com.dawn.common.annotation.TokenAnnotation;
import com.dawn.common.result.Result;
import com.dawn.common.result.ResultData;
import com.dawn.system.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * <p>script-control-com.dawn.arknight.controller-JobController</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@RestController
@RequestMapping("/arknight/job")
public class JobController {
    @Autowired
    IJobService jobService;
    @Autowired
    IUserService userService;

    /**
     * 获取今日任务列表
     *
     * @param request 请求体
     * @return 今日任务列表
     */
    @TokenAnnotation(permissionType = "job", permissionName = "list")
    @PostMapping("/get_todayList")
    public Result<ResultData> todayList(HttpServletRequest request) {
        String token = request.getHeader("token");

        return Result.ok(jobService.getTodayJobById(userService.getUserIdByToken(token)));
    }

    /**
     * @param request      请求体
     * @param jobId        任务id
     * @param startTime    开始日期
     * @param endTime      结束日期
     * @param jobFriend    好友任务
     * @param jobRhode     罗德岛
     * @param jobOverKill  剿灭
     * @param jobMaterials 材料
     * @return 结果
     */
    @TokenAnnotation(permissionType = "job", permissionName = "add")
    @PostMapping("/put")
    public Result<String> add(HttpServletRequest request, @RequestParam(required = false) Long jobId, @RequestParam(required = false) Date startTime, @RequestParam(required = false) Date endTime, @RequestParam(required = false) Boolean jobFriend, @RequestParam(required = false) Boolean jobRhode, @RequestParam(required = false) Boolean jobOverKill, @RequestParam(value = "jobMaterials", required = false) List<JobMaterial> jobMaterials, @RequestBody(required = false) JobSaveDTO job) {
        Long userId = userService.getUserIdByToken(request.getHeader("token"));
        if (job == null) {
            job = new JobSaveDTO(userId, jobId, startTime, endTime, jobFriend, jobRhode, jobOverKill, jobMaterials);
        }
        job.setUserId(userId);
        System.out.println("************************");
        System.out.println(job);
        System.out.println("************************");

        if (job.getJobId() != null) {
            if (jobService.countJob(job) == 0) {
                return Result.failed("该用户名下无此任务");
            }
        }
        return Result.okOrFailed(jobService.saveOrUpdateJob(job), "任务保存失败", "任务保存成功");

    }


}
