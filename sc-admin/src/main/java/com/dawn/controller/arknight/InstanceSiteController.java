package com.dawn.controller.arknight;

import com.dawn.arknight.domain.InstanceSite;
import com.dawn.arknight.service.IInstanceSiteService;
import com.dawn.common.annotation.TokenAnnotation;
import com.dawn.common.result.Result;
import com.dawn.common.util.file.image.ImageTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>script-control-com.dawn.arknight.controller-InstanceSiteController</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */
@RestController
@RequestMapping("/arknight/instance/site")
public class InstanceSiteController {
    @Autowired
    IInstanceSiteService instanceSiteService;

    /**
     * 添加副本地址属性
     *
     * @param instanceId    副本id
     * @param siteType      地址类型0：图片 1：坐标
     * @param siteValue     地址值
     * @param offsetAddress 偏移量
     * @param image         图片
     * @return 结果
     */
    @TokenAnnotation(permissionName = "instance", permissionType = "add")
    @PostMapping("/add")
    public Result<String> setInstanceSite(Long instanceId, Integer siteType, String siteValue, Integer offsetAddress, MultipartFile image) {

        InstanceSite instanceSite = new InstanceSite();
        instanceSite.setInstanceId(instanceId);
        instanceSite.setSiteType(siteType);
        instanceSite.setOffsetAddress(offsetAddress);
        System.out.println(instanceSite);
        //0为坐标
        if (siteType == 0) {
            instanceSite.setSiteValue(siteValue);
        } else if (siteType == 1) {
            instanceSite.setSiteValue(ImageTool.imageUpload(image, "instance", String.valueOf(instanceId)));
        } else {
            return Result.error("地址类型错误");
        }
        return Result.okOrFailed(instanceSiteService.saveOrUpdateByIds(instanceSite), "保存成功", "保存失败");

    }


    @PostMapping("/get_siteListByType")
    public Result<List<String>> siteListByType(@RequestBody(required = false) Integer typeJson, @RequestParam(required = false) Integer type) {
        if (type == null) {
            type = typeJson;
        }
        return Result.ok(instanceSiteService.siteListByType(type));
    }


}
