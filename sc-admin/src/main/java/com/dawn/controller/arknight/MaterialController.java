package com.dawn.controller.arknight;

import com.dawn.arknight.domain.Material;
import com.dawn.arknight.service.IMaterialService;
import com.dawn.common.annotation.TokenAnnotation;
import com.dawn.common.base.BaseController;
import com.dawn.common.result.Result;
import com.dawn.common.result.ResultData;
import com.dawn.common.util.file.image.ImageTool;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>script-control-com.dawn.controller-MaterialController</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */
@Api(tags = "材料模块")
@RestController
@RequestMapping("/arknight/material")
public class MaterialController extends BaseController {

    @Autowired
    private IMaterialService materialService;

    @TokenAnnotation(permissionName = "material", permissionType = "list")
    @PostMapping("/list")
    @ApiOperation(value = "材料列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "material", value = "材料", dataTypeClass = Material.class, paramType = "body"),
    })
    public Result<ResultData> list(Material material) {

        System.out.println(material);
        return Result.ok(materialService.listMaterial(material));
    }

    @TokenAnnotation(permissionName = "material", permissionType = "get")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "材料标识", required = true, dataTypeClass = Long.class, paramType = "body"),
    })
    @ApiOperation(value = "获取材料")
    @GetMapping("/getById/{materialId}")
    public Result<Material> getById(@PathVariable String materialId) {
        return Result.ok(materialService.getById(materialId));
    }

    @TokenAnnotation(permissionName = "material", permissionType = "add")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "material", value = "材料", required = true, dataTypeClass = Material.class, paramType = "body"),
    })
    @ApiOperation(value = "添加材料")
    @PostMapping("/add")
    public Result<String> add(String materialName, Integer materialRarity, MultipartFile image) {
        Material material = new Material();
        material.setMaterialName(materialName);
        material.setMaterialRarity(materialRarity);
        material.setMaterialPicture(ImageTool.imageUpload(image, "material", materialName));
        return materialService.save(material) ? Result.ok() : Result.error("添加失败");
    }

    @TokenAnnotation(permissionName = "material", permissionType = "edit")
    @ApiOperation(value = "修改材料")
    @PostMapping("/edit")
    public Result<String> edit(Long materialId, String materialName, Integer materialRarity, MultipartFile image) {
        Material material = new Material();
        material.setMaterialId(materialId);
        material.setMaterialName(materialName);
        material.setMaterialRarity(materialRarity);
        material.setMaterialPicture(ImageTool.imageUpload(image, "material", materialName));
        return materialService.updateById(material) ? Result.ok() : Result.error("修改失败");
    }

    @TokenAnnotation(permissionName = "material", permissionType = "remove")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "材料标识", required = true, dataTypeClass = Long.class, paramType = "body"),
    })
    @ApiOperation(value = "删除材料")
    @GetMapping("/removeById/{id}")
    public Result<String> removeById(@PathVariable String id) {
        return materialService.removeById(id) ? Result.ok() : Result.error("删除失败");
    }


}
