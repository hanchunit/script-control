package com.dawn.controller.arknight;

import com.dawn.arknight.domain.Instance;
import com.dawn.arknight.service.IInstanceService;
import com.dawn.common.annotation.TokenAnnotation;
import com.dawn.common.base.BaseController;
import com.dawn.common.result.Result;
import com.dawn.common.result.ResultData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;

/**
 * 副本Controller
 *
 * @author HanChun
 * @date 2021-06-12
 */
@Api(tags = "副本模块")
@RestController
@RequestMapping("/arknight/instance")
public class InstanceController extends BaseController {
    @Autowired
    private IInstanceService instanceService;

    @TokenAnnotation(permissionName = "instance", permissionType = "list")
    @ApiOperation(value = "副本列表")

    @PostMapping("/list")
    public Result<ResultData> list(Instance instance) {
        return Result.ok(instanceService.selectInstanceList(instance));
    }

    @TokenAnnotation(permissionName = "instance", permissionType = "get")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "副本标识", required = true, dataTypeClass = Long.class, paramType = "body"),
    })
    @ApiOperation("获取副本")
    @GetMapping("/getById/{id}")
    public Result<Instance> getById(@PathVariable Serializable id) {
        return Result.ok(instanceService.getById(id));
    }

    /**
     * 添加副本
     *
     * @param instanceName   副本名称
     * @param instanceEnergy 消耗体力
     * @return 结果
     */
    @TokenAnnotation(permissionName = "instance", permissionType = "add")
    @ApiOperation("添加副本")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "instance", value = "副本", required = true, dataTypeClass = Instance.class, paramType = "body"),
    })
    @PostMapping("/add")
    public Result<String> add(String instanceName, Integer instanceEnergy) {
        Instance instance = new Instance();
        instance.setInstanceEnergy(instanceEnergy);
        instance.setInstanceName(instanceName);
        return instanceService.save(instance) ? Result.ok() : Result.failed("添加失败");
    }

    @TokenAnnotation(permissionName = "instance", permissionType = "edit")
    @ApiOperation("修改副本")
    @PostMapping("/edit")
    public Result<String> edit(Long instanceId, String instanceName, Integer instanceEnergy) {
        Instance instance = new Instance();
        instance.setInstanceId(instanceId);
        instance.setInstanceEnergy(instanceEnergy);
        instance.setInstanceName(instanceName);
        return instanceService.updateById(instance) ? Result.ok() : Result.failed("修改失败");
    }

    @TokenAnnotation(permissionName = "instance", permissionType = "remove")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "副本标识", required = true, dataTypeClass = Long.class, paramType = "body"),
    })
    @ApiOperation("删除副本")
    @GetMapping("/removeById/{id}")
    public Result<String> removeById(@PathVariable String id) {
        return instanceService.removeById(id) ? Result.ok() : Result.failed("删除失败");
    }

}
