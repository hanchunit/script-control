package com.dawn.controller.arknight;

import com.dawn.arknight.domain.dto.LogDTO;
import com.dawn.arknight.domain.dto.LogSaveDTO;
import com.dawn.arknight.domain.vo.LogMaterialVO;
import com.dawn.arknight.service.ILogService;
import com.dawn.common.annotation.TokenAnnotation;
import com.dawn.common.result.Result;
import com.dawn.common.result.ResultData;
import com.dawn.system.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>script-control-com.dawn.arknight.controller-LogController</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@RestController
@RequestMapping("/job/log")
public class LogController {
    @Autowired
    private ILogService logService;
    @Autowired
    private IUserService userService;

    @PostMapping("get_list")
    @TokenAnnotation(permissionName = "jobLog", permissionType = "list")
    public Result<ResultData> list(HttpServletRequest request) {
        String token = request.getHeader("token");
        return Result.ok(logService.selectJobLog(userService.getUserIdByToken(token)));
    }

    @PostMapping("get_newLog")
    @TokenAnnotation(permissionName = "jobLog", permissionType = "list")
    public Result<LogDTO> newLog(HttpServletRequest request, @RequestParam(required = false) @RequestBody(required = false) Boolean jobState) {
        String token = request.getHeader("token");
        LogDTO logDTO = new LogDTO();
        logDTO.setUserId(userService.getUserIdByToken(token));
        logDTO.setJobState(jobState);
        return Result.ok(logService.getNewJobLog(logDTO));
    }


    @PostMapping("put_Log")
    @TokenAnnotation(permissionName = "jobLog", permissionType = "put")
    public Result<LogDTO> putLog(HttpServletRequest request, @RequestParam(required = false) Boolean jobState, @RequestParam(required = false) String jobMessage, @RequestParam(required = false) Integer instance, @RequestParam(required = false) List<LogMaterialVO> materials, @RequestBody(required = false) LogSaveDTO log) {
        String token = request.getHeader("token");
        if (instance != null) {
            log.setInstance(instance);
        }
        if (jobState != null) {
            log.setJobState(jobState);
        }
        if (jobMessage != null) {
            log.setJobMessage(jobMessage);
        }
        if (materials != null) {
            log.setMaterials(materials);
        }
        log.setUserId(userService.getUserIdByToken(token));


        return Result.okOrFailed(logService.saveOrUpdateJobLog(log), "日志保存成功", "日志保存失败");
    }


}
