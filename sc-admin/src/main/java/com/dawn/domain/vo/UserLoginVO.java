package com.dawn.domain.vo;

import com.dawn.common.base.BaseEntity;
import lombok.Data;

/**
 * <p>script-control-com.dawn.domain.vo-UserLoginVO</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Data
public class UserLoginVO extends BaseEntity {
    private static final long serialVersionUID = 7434070893682212875L;

    private String username;

    private String password;


}
