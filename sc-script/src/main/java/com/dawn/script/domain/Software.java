package com.dawn.script.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.dawn.common.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>script-control-com.dawn.domain-Software</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Data
@TableName("script_software")
public class Software extends BaseEntity {
    private static final long serialVersionUID = 5767557609212904285L;
    @TableId
    private Long softwareId;

    private String downloadUrl;

    private String version;

    private Boolean dialog;

    private String msg;

    private Boolean force;

    private String softwareUrl;

    @JsonValue
    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("download_url", getDownloadUrl());
        map.put("version", getVersion());
        map.put("dialog", getDialog());
        map.put("msg", getMsg());
        map.put("force", getForce());
        map.put("software_url", getSoftwareUrl());
        return map;
    }
}
