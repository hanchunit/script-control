package com.dawn.script.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dawn.script.domain.Software;
import com.dawn.script.mapper.SoftwareMapper;
import com.dawn.script.service.ISoftwareService;
import org.springframework.stereotype.Service;

/**
 * <p>script-control-com.dawn.script.service.impl-SoftwareServiceImpl</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Service
public class SoftwareServiceImpl extends ServiceImpl<SoftwareMapper, Software> implements ISoftwareService {
    @Override
    public Software newestSoftware() {
        return getBaseMapper().newestSoftware();
    }

    @Override
    public Boolean saveOrUpdateSoftware(Software software) {
        return getBaseMapper().saveOrUpdateSoftware(software);
    }
}
