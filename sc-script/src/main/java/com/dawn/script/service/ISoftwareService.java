package com.dawn.script.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dawn.script.domain.Software;

/**
 * <p>script-control-com.dawn.script.service.impl-ISoftwareService</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */
public interface ISoftwareService extends IService<Software> {


    public Software newestSoftware();


    public Boolean saveOrUpdateSoftware(Software software);
}
