package com.dawn.script.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dawn.script.domain.Software;
import org.apache.ibatis.annotations.Mapper;

import javax.annotation.ManagedBean;

/**
 * <p>script-control-com.dawn.script.mapper-SoftwareMapper</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */
@Mapper
public interface SoftwareMapper extends BaseMapper<Software> {
    public Software newestSoftware();

    public Boolean saveOrUpdateSoftware(Software software);
}
