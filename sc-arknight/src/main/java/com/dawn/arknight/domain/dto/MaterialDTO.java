package com.dawn.arknight.domain.dto;

import com.dawn.arknight.domain.vo.MaterialInstanceVO;
import com.dawn.common.base.BaseEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * <p>script-control-com.dawn.domain.dto-MaterialDTO</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */
@Api(value = "材料dto")
@Data
public class MaterialDTO extends BaseEntity {
    private static final long serialVersionUID = 7610668058517328186L;

    /**
     * 材料标识
     */
    @ApiModelProperty("材料标识")
    private Long materialId;

    /**
     * 材料名称
     */

    @ApiModelProperty("材料名称")
    private String materialName;

    /**
     * 稀有度
     */

    @ApiModelProperty("稀有度")
    private Integer materialRarity;

    /**
     * 图片地址
     */

    @ApiModelProperty("图片地址")
    private String materialPicture;

    @ApiModelProperty("掉落材料的副本")
    private List<MaterialInstanceVO> materialInstances;
}
