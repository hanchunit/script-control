package com.dawn.arknight.domain.dto;

import com.dawn.arknight.domain.vo.InstanceMaterialVO;
import com.dawn.arknight.domain.vo.InstanceSiteVO;
import com.dawn.common.base.BaseEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * <p>script-control-com.dawn.domain.dto-MaterialDTO</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */
@Api(value = "副本dto")
@Data
public class InstanceDTO extends BaseEntity {


    private static final long serialVersionUID = -329469684505873828L;

    /**
     * 副本标识
     */

    @ApiModelProperty(value = "副本标识")
    private Long instanceId;

    /**
     * 副本名称
     */
    @ApiModelProperty(value = "副本名称")
    private String instanceName;

    /**
     * 消耗体力
     */
    @ApiModelProperty(value = "消耗体力")
    private Integer instanceEnergy;
    /**
     * 副本地址
     */
    @ApiModelProperty(value = "副本地址")
    private List<InstanceSiteVO> instanceSites;


    @ApiModelProperty(value = "副本掉落材料")
    private List<InstanceMaterialVO> instanceMaterials;
}
