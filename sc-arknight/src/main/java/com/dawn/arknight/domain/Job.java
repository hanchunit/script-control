package com.dawn.arknight.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.dawn.common.base.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * <p>script-control-com.dawn.arknight.mapper-Job</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Data
@TableName("arknight_job")
public class Job extends BaseEntity {
    private static final long serialVersionUID = 1535056148005639385L;
    @TableId
    private Long jobId;

    private Date startTime;

    private Date endTime;

    private Integer jobState;

    private Boolean jobRhode;

    private Boolean jobFriend;

    private Boolean jobOverKill;


}
