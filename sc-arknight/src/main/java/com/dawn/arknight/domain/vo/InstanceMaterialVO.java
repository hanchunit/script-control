package com.dawn.arknight.domain.vo;

import com.dawn.common.base.BaseEntity;
import lombok.Data;

/**
 * <p>script-control-com.dawn.domain.vo-MaterialInstanceVO</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Data
public class InstanceMaterialVO extends BaseEntity {

    private static final long serialVersionUID = -5925152571360557750L;
    private Long materialId;

    /**
     * 材料名称
     */

    private String materialName;

    /**
     * 稀有度
     */

    private Integer materialRarity;

    /**
     * 图片地址
     */

    private String materialPicture;


    /**
     * 掉落概率0-100%
     */
    private Integer dropProbability;

    /**
     * 掉落数量
     */
    private Integer dropNumber;
}
