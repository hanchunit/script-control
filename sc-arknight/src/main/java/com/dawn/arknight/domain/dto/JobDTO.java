package com.dawn.arknight.domain.dto;

import com.dawn.arknight.domain.vo.JobMaterial;
import com.dawn.common.base.BaseEntity;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * <p>script-control-com.dawn.arknight.domain.dto-JobDTO</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Data
public class JobDTO extends BaseEntity {
    private static final long serialVersionUID = -4444841485474645625L;

    private Long userId;

    private Long jobId;

    private Integer jobState;

    private Date startTime;


    private Date endTime;

    private Boolean jobFriend;

    private Boolean jobRhode;

    private Boolean jobOverKill;

    private List<JobMaterial> jobMaterials;


}
