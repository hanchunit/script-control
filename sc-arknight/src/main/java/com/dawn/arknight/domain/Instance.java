package com.dawn.arknight.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.dawn.common.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * 副本表(instance)
 *
 * @author hanchun
 * @version 1.0.0 2021-06-12
 */
@Data
@TableName("arknight_instance")
@ApiModel(value = "副本")
public class Instance extends BaseEntity {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 5273750031554230827L;


    /**
     * 副本标识
     */
    @TableId
    @ApiModelProperty(value = "副本标识")
    private Long instanceId;

    /**
     * 副本名称
     */
    @ApiModelProperty(value = "副本名称")
    private String instanceName;

    /**
     * 消耗体力
     */
    @ApiModelProperty(value = "副本消耗体力")
    private Integer instanceEnergy;


    @JsonValue
    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("instanceId", getInstanceId());
        map.put("instanceName", getInstanceName());
        map.put("instanceEnergy", getInstanceEnergy());


        return map;
    }

}