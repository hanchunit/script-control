package com.dawn.arknight.domain.vo;

import com.dawn.common.base.BaseEntity;
import lombok.Data;

/**
 * <p>script-control-com.dawn.arknight.domain-LogMaterial</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Data
public class LogMaterialVO extends BaseEntity {
    private static final long serialVersionUID = -2228360359658417918L;
    /**
     * 材料id
     */
    private Long materialId;
    /**
     * 已完成数量
     */
    private Integer gainNumber;

}
