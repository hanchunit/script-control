package com.dawn.arknight.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.dawn.common.base.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * <p>script-control-com.dawn.arknight.domain-JobLog</p>
 * <p>Description: 任务日志 </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Data
@TableName("arknight_log")
public class Log extends BaseEntity {
    private static final long serialVersionUID = 9186088457665016074L;

    @TableId
    private Long logId;

    private Date logTime;

    private Boolean jobState;


    private String jobMessage;

    private Long instanceId;

}
