package com.dawn.arknight.domain.dto;

import com.dawn.arknight.domain.vo.LogMaterialVO;
import com.dawn.common.base.BaseEntity;
import lombok.Data;

import java.util.List;

/**
 * <p>script-control-com.dawn.arknight.domain.dto-LogSaveDTO</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Data
public class LogSaveDTO extends BaseEntity {
    private static final long serialVersionUID = 4701067385843491895L;

    /**
     * 用户id
     */
    private Long userId;
    /**
     * 任务id
     */
    private Long jobId;

    private Long logId;
    /**
     * 运行状态
     */
    private Boolean jobState;
    /**
     * 任务消息
     */
    private String jobMessage;
    /**
     * 副本
     */
    private Integer instance;
    /**
     * 掉落材料
     */
    private List<LogMaterialVO> materials;
}
