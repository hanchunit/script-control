package com.dawn.arknight.domain.vo;

import com.dawn.common.base.BaseEntity;
import lombok.Data;

/**
 * <p>script-control-com.dawn.arknight.domain.vo-JobMaterial</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Data
public class JobMaterial extends BaseEntity {
    private static final long serialVersionUID = -7177682162467192332L;
    /**
     * 材料id
     */
    private Long materialId;
    /**
     * 材料名称
     */
    private String materialName;
    /**
     * 材料稀有度
     */
    private Integer materialRarity;
    /**
     * 图片地址
     */

    private String materialPicture;
    /**
     * 已完成数量
     */
    private Integer completionQuantity;
    /**
     * 计划数量
     */
    private Integer plannedQuantity;
    /**
     * 材料状态 0：未完成，1：已完成
     */
    private Boolean materialState;

}
