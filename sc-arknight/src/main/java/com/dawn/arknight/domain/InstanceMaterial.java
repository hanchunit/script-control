package com.dawn.arknight.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.dawn.common.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * 副本材料表(instance_material)
 *
 * @author hanchun
 * @version 1.0.0 2021-06-12
 */
@Data
@TableName("arknight_instance_material")
@Api(value = "副本材料中间")
public class InstanceMaterial extends BaseEntity {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 5812473064193012421L;


    /**
     * 副本标识
     */
    @TableId
    @ApiModelProperty(value = "副本标识")
    private Long instanceId;

    /**
     * 材料标识
     */
    @ApiModelProperty(value = "材料标识")
    private Long materialId;

    /**
     * 掉落概率0-100%
     */
    @ApiModelProperty(value = "掉落概率")
    private Integer dropProbability;

    /**
     * 掉落数量
     */
    @ApiModelProperty(value = "掉落数量")
    private Integer dropNumber;

    @JsonValue
    public Map<String, String> toMap() {
        Map<String, String> map = new HashMap<>();
        map.put("instanceId", getInstanceId().toString());
        map.put("materialId", getMaterialId().toString());
        map.put("dropProbability", getDropProbability() + "%");
        map.put("dropNumber", getDropNumber().toString());
        return map;
    }

}