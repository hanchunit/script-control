package com.dawn.arknight.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.dawn.common.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * 材料表(material)
 *
 * @author hanchun
 * @version 1.0.0 2021-06-12
 */
@Data
@TableName("arknight_material")
@Api(value = "材料")
public class Material extends BaseEntity {
    /**
     * 版本号
     */
    private static final long serialVersionUID = -7768558032735608652L;


    /**
     * 材料标识
     */
    @ApiModelProperty(value = "材料标识")
    @TableId
    private Long materialId;

    /**
     * 材料名称
     */

    @ApiModelProperty(value = "材料名称")
    private String materialName;

    /**
     * 稀有度
     */

    @ApiModelProperty(value = "稀有度")
    private Integer materialRarity;

    /**
     * 图片地址
     */

    @ApiModelProperty(value = "图片地址")
    private String materialPicture;

    @JsonValue
    public Map<String, String> toMap() {
        Map<String, String> map = new HashMap<>();
        map.put("materialId", getMaterialId().toString());
        map.put("materialName", getMaterialName());
        map.put("materialRarity", getMaterialRarity().toString());
        map.put("materialPicture", getMaterialPicture());
        return map;
    }
}