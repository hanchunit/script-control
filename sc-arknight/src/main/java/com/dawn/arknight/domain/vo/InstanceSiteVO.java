package com.dawn.arknight.domain.vo;

import com.dawn.common.base.BaseEntity;
import lombok.Data;

/**
 * <p>script-control-com.dawn.arknight.domain.vo-InstanceSiteVO</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Data
public class InstanceSiteVO extends BaseEntity {
    private static final long serialVersionUID = -3321269521303926069L;


    /**
     * 地址类型
     */
    private Integer siteType;
    /**
     * 地址值
     */

    private String siteValue;
    /**
     * 地址偏移量
     */

    private String offsetAddress;


}
