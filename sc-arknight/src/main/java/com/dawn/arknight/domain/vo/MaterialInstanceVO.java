package com.dawn.arknight.domain.vo;

import com.dawn.common.base.BaseEntity;
import lombok.Data;

import java.util.List;

/**
 * <p>script-control-com.dawn.domain.vo-MaterialInstanceVO</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Data
public class MaterialInstanceVO extends BaseEntity {
    private static final long serialVersionUID = -5939791219725636415L;

    /**
     * 副本标识
     */

    private Long instanceId;

    /**
     * 副本名称
     */
    private String instanceName;

    /**
     * 消耗体力
     */
    private Integer instanceEnergy;
    /**
     * 副本地址
     */
    private List<InstanceSiteVO> instanceSites;


    /**
     * 掉落概率0-100%
     */
    private Integer dropProbability;

    /**
     * 掉落数量
     */
    private Integer dropNumber;
}
