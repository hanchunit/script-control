package com.dawn.arknight.domain.vo;

import com.dawn.common.base.BaseEntity;
import lombok.Data;

/**
 * <p>script-control-com.dawn.arknight.domain-LogMaterial</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Data
public class LogMaterial extends BaseEntity {
    private static final long serialVersionUID = -2228360359658417918L;
    /**
     * 任务id
     */
    private Long logId;
    /**
     * 材料id
     */
    private Long materialId;
    /**
     * 材料名称
     */
    private String materialName;
    /**
     * 材料稀有度
     */
    private Integer materialRarity;
    /**
     * 图片地址
     */
    private String materialPicture;
    /**
     * 已完成数量
     */
    private Integer gainNumber;

}
