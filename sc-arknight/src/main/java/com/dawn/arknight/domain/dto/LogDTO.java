package com.dawn.arknight.domain.dto;

import com.dawn.arknight.domain.Instance;
import com.dawn.arknight.domain.vo.LogMaterial;
import com.dawn.common.base.BaseEntity;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * <p>script-control-com.dawn.arknight.domain.dto-LogDTO</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Data
public class LogDTO extends BaseEntity {
    private static final long serialVersionUID = 7242359706995936821L;
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 任务id
     */
    private Long jobId;
    /**
     * 日志id
     */
    private Long logId;
    /**
     * 日志时间
     */
    private Date logTime;
    /**
     * 运行状态
     */
    private Boolean jobState;
    /**
     * 任务消息
     */
    private String jobMessage;
    /**
     * 副本
     */
    private Instance instance;
    /**
     * 掉落材料
     */
    private List<LogMaterial> materials;
}
