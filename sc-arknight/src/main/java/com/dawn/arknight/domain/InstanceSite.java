package com.dawn.arknight.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.dawn.common.base.BaseEntity;
import lombok.Data;

/**
 * <p>script-control-com.dawn.arknight.domain-InstanceSite</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Data
@TableName("arknight_instance_site")
public class InstanceSite extends BaseEntity {
    private static final long serialVersionUID = 1583692006862335354L;
    /**
     * 副本id
     */
    @TableId(type = IdType.INPUT)
    private Long instanceId;

    /**
     * 地址类型
     */
    private Integer siteType;
    /**
     * 地址值
     */

    private String siteValue;
    /**
     * 地址偏移量
     */

    private Integer offsetAddress;

}
