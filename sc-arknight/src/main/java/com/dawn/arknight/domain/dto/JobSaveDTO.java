package com.dawn.arknight.domain.dto;

import com.dawn.arknight.domain.vo.JobMaterial;
import com.dawn.common.base.BaseEntity;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * <p>script-control-com.dawn.arknight.domain.dto-JobSaveDTO</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Data
public class JobSaveDTO extends BaseEntity {
    private static final long serialVersionUID = 6211167480942187632L;

    private Long userId;
    private Long jobId;
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:ss")
    private Date startTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:ss")
    private Date endTime;

    private Boolean jobFriend;

    private Boolean jobRhode;

    private Boolean jobOverKill;

    private List<JobMaterial> jobMaterials;

    public JobSaveDTO(Long userId, Long jobId, Date startTime, Date endTime, Boolean jobFriend, Boolean jobRhode, Boolean jobOverKill, List<JobMaterial> jobMaterials) {
        this.userId = userId;
        this.jobId = jobId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.jobFriend = jobFriend;
        this.jobRhode = jobRhode;
        this.jobOverKill = jobOverKill;
        this.jobMaterials = jobMaterials;
    }
}
