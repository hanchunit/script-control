package com.dawn.arknight.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dawn.arknight.domain.InstanceMaterial;

/**
 * 副本材料Service接口
 *
 * @author HanChun
 * @date 2021-06-12
 */
public interface IInstanceMaterialService extends IService<InstanceMaterial> {

}
