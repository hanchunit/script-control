package com.dawn.arknight.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dawn.arknight.domain.Instance;
import com.dawn.common.result.ResultData;

/**
 * 副本Service接口
 *
 * @author HanChun
 * @date 2021-06-12
 */
public interface IInstanceService extends IService<Instance> {
    ResultData selectInstanceList(Instance instance);
}