package com.dawn.arknight.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dawn.arknight.domain.InstanceSite;
import com.dawn.arknight.mapper.InstanceSiteMapper;
import com.dawn.arknight.service.IInstanceSiteService;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

/**
 * <p>script-control-com.dawn.arknight.service.impl-InstanceSiteServiceImpl</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Service
public class InstanceSiteServiceImpl extends ServiceImpl<InstanceSiteMapper, InstanceSite> implements IInstanceSiteService {
    @Override
    public Boolean selectOneByIds(InstanceSite instanceSite) {
        return getBaseMapper().selectOneByIds(instanceSite);
    }

    @Override
    public Boolean updateOneByIds(InstanceSite instanceSite) {
        return getBaseMapper().updateOneByIds(instanceSite);
    }

    @Override
    public Boolean saveOrUpdateByIds(InstanceSite instanceSite) {
        if (selectOneByIds(instanceSite)) {
            return updateOneByIds(instanceSite);
        } else {
            return save(instanceSite);
        }

    }

    @Override
    public List<String> siteListByType(Serializable siteType) {
        return getBaseMapper().siteListByType(siteType);
    }

    @Override
    public List<String> siteImages() {
        return getBaseMapper().siteImages();
    }
}
