package com.dawn.arknight.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dawn.arknight.domain.Material;
import com.dawn.common.result.ResultData;

/**
 * <p>script-control-com.dawn.script.service-IMaterialService</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

public interface IMaterialService extends IService<Material> {
    ResultData listMaterial(Material material);
}
