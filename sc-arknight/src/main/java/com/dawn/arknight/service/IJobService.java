package com.dawn.arknight.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dawn.arknight.domain.Job;
import com.dawn.arknight.domain.dto.JobSaveDTO;
import com.dawn.common.result.ResultData;

import java.io.Serializable;

/**
 * <p>script-control-com.dawn.arknight.service-IJobService</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */
public interface IJobService extends IService<Job> {
    public ResultData getTodayJobById(Serializable id);

    public Boolean saveOrUpdateJob(JobSaveDTO job);

    public Integer countJob(JobSaveDTO job);
}
