package com.dawn.arknight.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dawn.arknight.domain.Log;
import com.dawn.arknight.domain.dto.LogDTO;
import com.dawn.arknight.domain.dto.LogSaveDTO;
import com.dawn.common.result.ResultData;

import java.io.Serializable;

/**
 * <p>script-control-com.dawn.arknight.service-ILogService</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */
public interface ILogService extends IService<Log> {
    public ResultData selectJobLog(Serializable id);

    public LogDTO getNewJobLog(LogDTO logDTO);

    public Boolean saveOrUpdateJobLog(LogSaveDTO job);

}
