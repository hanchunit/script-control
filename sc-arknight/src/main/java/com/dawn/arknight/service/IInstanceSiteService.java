package com.dawn.arknight.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dawn.arknight.domain.InstanceSite;

import java.io.Serializable;
import java.util.List;

/**
 * <p>script-control-com.dawn.arknight.service-IInstanceSiteService</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public interface IInstanceSiteService extends IService<InstanceSite> {
    public Boolean selectOneByIds(InstanceSite instanceSite);

    public Boolean updateOneByIds(InstanceSite instanceSite);

    public Boolean saveOrUpdateByIds(InstanceSite instanceSite);

    public List<String> siteListByType(Serializable siteType);

    public List<String> siteImages();
}
