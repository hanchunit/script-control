package com.dawn.arknight.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dawn.arknight.domain.Job;
import com.dawn.arknight.domain.dto.JobSaveDTO;
import com.dawn.arknight.mapper.JobMapper;
import com.dawn.arknight.service.IJobService;
import com.dawn.common.result.ResultData;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * <p>script-control-com.dawn.arknight.service.impl-JobServiceImpl</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Service
public class JobServiceImpl extends ServiceImpl<JobMapper, Job> implements IJobService {
    @Override
    public ResultData getTodayJobById(Serializable id) {
        return new ResultData(getBaseMapper().getTodayJobById(id));
    }

    @Override
    public Boolean saveOrUpdateJob(JobSaveDTO job) {
        return getBaseMapper().saveJob(job) &&
                getBaseMapper().saveUserJob(job) &&
                getBaseMapper().saveJobMaterials(job);

    }

    @Override
    public Integer countJob(JobSaveDTO job) {
        return getBaseMapper().countJob(job);
    }
}
