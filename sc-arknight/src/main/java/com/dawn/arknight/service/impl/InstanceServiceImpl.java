package com.dawn.arknight.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dawn.arknight.domain.Instance;
import com.dawn.arknight.mapper.InstanceMapper;
import com.dawn.arknight.service.IInstanceService;
import com.dawn.common.result.ResultData;
import org.springframework.stereotype.Service;

/**
 * 副本Service业务层处理
 *
 * @author HanChun
 * @date 2021-06-12
 */
@Service
public class InstanceServiceImpl extends ServiceImpl<InstanceMapper, Instance> implements IInstanceService {
    @Override
    public ResultData selectInstanceList(Instance instance) {
        return new ResultData(getBaseMapper().selectInstanceList(instance));
    }
}
