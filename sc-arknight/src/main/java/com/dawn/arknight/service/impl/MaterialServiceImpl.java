package com.dawn.arknight.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dawn.arknight.domain.Material;
import com.dawn.arknight.mapper.MaterialMapper;
import com.dawn.arknight.service.IMaterialService;
import com.dawn.common.result.ResultData;
import org.springframework.stereotype.Service;

/**
 * <p>script-control-com.dawn.script.service.impl-MaterialServiceImpl</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Service
public class MaterialServiceImpl extends ServiceImpl<MaterialMapper, Material> implements IMaterialService {


    @Override
    public ResultData listMaterial(Material material) {

        return new ResultData(getBaseMapper().selectMaterialList(material));
    }
}
