package com.dawn.arknight.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dawn.arknight.domain.InstanceMaterial;
import com.dawn.arknight.mapper.InstanceMaterialMapper;
import com.dawn.arknight.service.IInstanceMaterialService;
import org.springframework.stereotype.Service;

/**
 * 副本材料Service业务层处理
 *
 * @author HanChun
 * @date 2021-06-12
 */
@Service
public class InstanceMaterialServiceImpl extends ServiceImpl<InstanceMaterialMapper, InstanceMaterial> implements IInstanceMaterialService {

}
