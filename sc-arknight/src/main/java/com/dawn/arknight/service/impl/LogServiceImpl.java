package com.dawn.arknight.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dawn.arknight.domain.Log;
import com.dawn.arknight.domain.dto.LogDTO;
import com.dawn.arknight.domain.dto.LogSaveDTO;
import com.dawn.arknight.mapper.LogMapper;
import com.dawn.arknight.service.ILogService;
import com.dawn.common.result.ResultData;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * <p>script-control-com.dawn.arknight.service.impl-LogServiceImpl</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Service
public class LogServiceImpl extends ServiceImpl<LogMapper, Log> implements ILogService {


    @Override
    public ResultData selectJobLog(Serializable id) {
        return new ResultData(getBaseMapper().selectJobLog(id));
    }

    @Override
    public LogDTO getNewJobLog(LogDTO logDTO) {
        return getBaseMapper().getNewJobLog(logDTO);
    }

    @Override
    public Boolean saveOrUpdateJobLog(LogSaveDTO log) {
        return getBaseMapper().saveOrUpdateLog(log) && getBaseMapper().saveJobLog(log) && getBaseMapper().saveLogMaterial(log);
    }
}
