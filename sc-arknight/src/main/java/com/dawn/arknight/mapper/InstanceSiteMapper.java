package com.dawn.arknight.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dawn.arknight.domain.InstanceSite;
import org.apache.ibatis.annotations.Mapper;

import java.io.Serializable;
import java.util.List;

/**
 * <p>script-control-com.dawn.arknight.mapper-InstanceSiteMapper</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Mapper
public interface InstanceSiteMapper extends BaseMapper<InstanceSite> {
    public boolean updateOneByIds(InstanceSite instanceSite);

    public boolean selectOneByIds(InstanceSite instanceSite);

    public List<String> siteListByType(Serializable siteType);

    public List<String> siteImages();
}
