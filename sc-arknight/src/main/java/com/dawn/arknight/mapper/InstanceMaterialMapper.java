package com.dawn.arknight.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dawn.arknight.domain.InstanceMaterial;
import org.apache.ibatis.annotations.Mapper;

/**
 * 副本材料Mapper接口
 *
 * @author HanChun
 * @date 2021-06-12
 */
@Mapper
public interface InstanceMaterialMapper extends BaseMapper<InstanceMaterial> {
}
