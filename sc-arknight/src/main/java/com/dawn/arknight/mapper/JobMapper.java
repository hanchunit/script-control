package com.dawn.arknight.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dawn.arknight.domain.Job;
import com.dawn.arknight.domain.dto.JobDTO;
import com.dawn.arknight.domain.dto.JobSaveDTO;
import org.apache.ibatis.annotations.Mapper;

import java.io.Serializable;
import java.util.List;

/**
 * <p>script-control-com.dawn.arknight.mapper-JobMapper</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Mapper
public interface JobMapper extends BaseMapper<Job> {
    public List<JobDTO> getTodayJobById(Serializable id);

    public Boolean saveJob(JobSaveDTO job);

    public Boolean saveUserJob(JobSaveDTO job);

    public Boolean saveJobMaterials(JobSaveDTO job);

    public Integer countJob(JobSaveDTO job);
}
