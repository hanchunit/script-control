package com.dawn.arknight.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dawn.arknight.domain.Log;
import com.dawn.arknight.domain.dto.LogDTO;
import com.dawn.arknight.domain.dto.LogSaveDTO;
import org.apache.ibatis.annotations.Mapper;

import java.io.Serializable;
import java.util.List;

/**
 * <p>script-control-com.dawn.arknight.mapper-LogMapper</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */
@Mapper
public interface LogMapper extends BaseMapper<Log> {
    public List<LogDTO> selectJobLog(Serializable id);

    public LogDTO getNewJobLog(LogDTO logDTO);

    /**
     * 保存或者更新日志
     *
     * @param logSaveDTO 日志保存实体
     * @return 是否保存成功
     */
    public Boolean saveOrUpdateLog(LogSaveDTO logSaveDTO);

    /**
     * 保存任务日志中间表
     *
     * @param logSaveDTO 日志保存实体
     * @return 是否保存成功
     */
    public Boolean saveJobLog(LogSaveDTO logSaveDTO);

    /**
     * 保存任务日志获取的材料
     *
     * @param logSaveDTO 日志保存实体
     * @return 是否保存成功
     */
    public Boolean saveLogMaterial(LogSaveDTO logSaveDTO);
}
