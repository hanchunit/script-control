package com.dawn.arknight.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dawn.arknight.domain.Instance;
import com.dawn.arknight.domain.dto.InstanceDTO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 副本Mapper接口
 *
 * @author HanChun
 * @date 2021-06-12
 */
@Mapper
public interface InstanceMapper extends BaseMapper<Instance> {
    public List<InstanceDTO> selectInstanceList(Instance instance);
}
