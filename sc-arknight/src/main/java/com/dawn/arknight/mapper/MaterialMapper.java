package com.dawn.arknight.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dawn.arknight.domain.Material;
import com.dawn.arknight.domain.dto.MaterialDTO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>script-control-com.dawn.mapper-MaterialMapper</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */
@Mapper
public interface MaterialMapper extends BaseMapper<Material> {
    public List<MaterialDTO> selectMaterialList(Material material);
}
