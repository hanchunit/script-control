package com.dawn.websocket.config;

import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

/**
 * <p>script-control-com.dawn.websocket.config-fffasd</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

public class SpringWebSocketHandler extends TextWebSocketHandler {

    @Override
    protected void handleTextMessage(WebSocketSession session,
                                     TextMessage message) throws Exception {
        String payload = message.getPayload();
        System.out.println("我接收到的消息：" + payload);

        String rtnMsg = "我回复了";

        for (int i = 0; i < 10; i++) {
            Thread.sleep(2000);
            session.sendMessage(new TextMessage(rtnMsg + i));
        }
        super.handleTextMessage(session, message);
    }
}