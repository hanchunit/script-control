/*
 Navicat Premium Data Transfer

 Source Server         : hanchun
 Source Server Type    : MySQL
 Source Server Version : 80023
 Source Host           : localhost:3306
 Source Schema         : script-control

 Target Server Type    : MySQL
 Target Server Version : 80023
 File Encoding         : 65001

 Date: 15/09/2021 20:36:32
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for arknight_instance
-- ----------------------------
DROP TABLE IF EXISTS `arknight_instance`;
CREATE TABLE `arknight_instance`  (
  `instance_id` bigint NOT NULL AUTO_INCREMENT COMMENT '副本标识',
  `instance_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '副本名称',
  `instance_energy` int NOT NULL COMMENT '消耗体力',
  PRIMARY KEY (`instance_id`) USING BTREE,
  UNIQUE INDEX `instance_instance_id_uindex`(`instance_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '副本表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for arknight_instance_material
-- ----------------------------
DROP TABLE IF EXISTS `arknight_instance_material`;
CREATE TABLE `arknight_instance_material`  (
  `instance_id` bigint UNSIGNED NOT NULL COMMENT '副本标识',
  `material_id` bigint UNSIGNED NOT NULL COMMENT '材料标识',
  `drop_probability` int NOT NULL COMMENT '掉落概率0-100%',
  `drop_number` int NOT NULL COMMENT '掉落数量',
  PRIMARY KEY (`instance_id`, `material_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '副本材料表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for arknight_instance_site
-- ----------------------------
DROP TABLE IF EXISTS `arknight_instance_site`;
CREATE TABLE `arknight_instance_site`  (
  `instance_id` bigint UNSIGNED NOT NULL COMMENT '副本id',
  `site_type` int UNSIGNED NOT NULL COMMENT '地址类型：0 坐标，1 图片',
  `site_value` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地址值：坐标及点颜色或图片地址',
  `offset_address` int UNSIGNED NULL DEFAULT NULL COMMENT '地图中偏移量',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`instance_id`, `site_type`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '副本图片' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for arknight_job
-- ----------------------------
DROP TABLE IF EXISTS `arknight_job`;
CREATE TABLE `arknight_job`  (
  `job_id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '任务id',
  `start_time` date NOT NULL COMMENT '开始时间',
  `end_time` date NOT NULL COMMENT '结束时间',
  `job_state` int UNSIGNED NULL DEFAULT 2 COMMENT '任务状态 0：已完成 1:未完成 2:进行中',
  `job_rhode` tinyint UNSIGNED NULL DEFAULT 1 COMMENT '基建罗德岛',
  `job_friend` tinyint UNSIGNED NULL DEFAULT 1 COMMENT '好友',
  `job_over_kill` tinyint UNSIGNED NULL DEFAULT 1 COMMENT '剿灭',
  PRIMARY KEY (`job_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for arknight_job_log
-- ----------------------------
DROP TABLE IF EXISTS `arknight_job_log`;
CREATE TABLE `arknight_job_log`  (
  `job_id` bigint UNSIGNED NOT NULL COMMENT '任务id',
  `log_id` bigint UNSIGNED NOT NULL COMMENT '日志id',
  PRIMARY KEY (`job_id`, `log_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for arknight_job_material
-- ----------------------------
DROP TABLE IF EXISTS `arknight_job_material`;
CREATE TABLE `arknight_job_material`  (
  `job_id` bigint UNSIGNED NOT NULL COMMENT '任务id',
  `material_id` bigint UNSIGNED NOT NULL COMMENT '材料id',
  `completion_quantity` int UNSIGNED NULL DEFAULT 0 COMMENT '完成数量',
  `planned_quantity` int UNSIGNED NOT NULL COMMENT '计划数量',
  `material_state` tinyint UNSIGNED NULL DEFAULT 0 COMMENT '材料状态 1：已完成 0：未完成',
  PRIMARY KEY (`job_id`, `material_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '任务材料' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for arknight_log
-- ----------------------------
DROP TABLE IF EXISTS `arknight_log`;
CREATE TABLE `arknight_log`  (
  `log_id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '日志id',
  `instance_id` bigint UNSIGNED NOT NULL COMMENT '副本名称',
  `log_time` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '日志时间',
  `job_state` tinyint UNSIGNED NOT NULL COMMENT '运行状态',
  `job_message` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务信息',
  PRIMARY KEY (`log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for arknight_log_material
-- ----------------------------
DROP TABLE IF EXISTS `arknight_log_material`;
CREATE TABLE `arknight_log_material`  (
  `log_id` bigint UNSIGNED NOT NULL COMMENT '日志id',
  `material_id` bigint UNSIGNED NOT NULL COMMENT '材料id',
  `gain_number` int UNSIGNED NOT NULL COMMENT '获得数量',
  PRIMARY KEY (`log_id`, `material_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for arknight_material
-- ----------------------------
DROP TABLE IF EXISTS `arknight_material`;
CREATE TABLE `arknight_material`  (
  `material_id` bigint NOT NULL AUTO_INCREMENT COMMENT '材料标识',
  `material_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '材料名称',
  `material_rarity` int NOT NULL COMMENT '稀有度',
  `material_picture` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图片地址',
  PRIMARY KEY (`material_id`) USING BTREE,
  UNIQUE INDEX `material_material_id_uindex`(`material_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '材料表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for arknight_user_job
-- ----------------------------
DROP TABLE IF EXISTS `arknight_user_job`;
CREATE TABLE `arknight_user_job`  (
  `user_id` bigint UNSIGNED NOT NULL COMMENT '用户id',
  `job_id` bigint UNSIGNED NOT NULL COMMENT '任务id',
  PRIMARY KEY (`user_id`, `job_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for script_software
-- ----------------------------
DROP TABLE IF EXISTS `script_software`;
CREATE TABLE `script_software`  (
  `software_id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '软件标识',
  `software_download_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新地址',
  `software_version` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '版本',
  `software_dialog` tinyint NULL DEFAULT NULL COMMENT '是否用对话框',
  `software_msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新消息',
  `software_force` tinyint NULL DEFAULT NULL COMMENT '是否强制更新',
  `software_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '下载地址',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`software_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '程序版本表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_device
-- ----------------------------
DROP TABLE IF EXISTS `sys_device`;
CREATE TABLE `sys_device`  (
  `device_id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '设备id',
  `device_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '设备名称',
  `device_imie` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '手机串码',
  `device_state` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '设备状态 0:正常 1:停用',
  `device_brand` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '设备型号',
  PRIMARY KEY (`device_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '设备' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission`  (
  `permission_id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '权限id',
  `permission_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限名称',
  `permission_type` varchar(14) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限类型',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`permission_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 102 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for sys_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_permission`;
CREATE TABLE `sys_role_permission`  (
  `role_id` bigint NOT NULL COMMENT '角色id',
  `permission_id` bigint NOT NULL COMMENT '权限id',
  PRIMARY KEY (`role_id`, `permission_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_serial
-- ----------------------------
DROP TABLE IF EXISTS `sys_serial`;
CREATE TABLE `sys_serial`  (
  `serial_id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '序列号id',
  `serial_number` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '序列号',
  `serial_state` int NOT NULL COMMENT '序列号状态 0:未使用 1:已使用',
  PRIMARY KEY (`serial_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '用户标识',
  `user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `login_name` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登录名',
  `login_password` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登录密码',
  `user_type` int UNSIGNED NULL DEFAULT 1 COMMENT '用户类型',
  `user_state` int NULL DEFAULT 0 COMMENT '用户状态\n0:正常 \n1:锁定\n2:销号',
  `user_token` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'token',
  `login_time` datetime NULL DEFAULT NULL COMMENT '登录时间',
  `user_device_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '账号激活的设备IMEI',
  PRIMARY KEY (`user_id`, `login_name`) USING BTREE,
  UNIQUE INDEX `sys_user_user_id_uindex`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_user_device
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_device`;
CREATE TABLE `sys_user_device`  (
  `user_id` bigint NOT NULL COMMENT '手机id',
  `device_id` bigint NOT NULL COMMENT '设备id',
  PRIMARY KEY (`user_id`, `device_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint NOT NULL COMMENT '用户id',
  `role_id` bigint NOT NULL COMMENT '角色id',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_user_serial
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_serial`;
CREATE TABLE `sys_user_serial`  (
  `user_id` bigint UNSIGNED NOT NULL COMMENT '用户id',
  `serial_id` bigint UNSIGNED NOT NULL COMMENT '序列号id',
  PRIMARY KEY (`user_id`, `serial_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
