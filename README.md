# ScriptControl

#### 介绍
对于手机上部分软件需要重复的操作进行任务式设置定时自动化执行(服务端)
#### 软件架构

- 后端采用spring boot+spring MVC+mybatis-plus搭建
- 数据库使用mysql8.0 
- 使用spring aop进行权限校验



#### 安装教程

1. 导入数据库script-control2021091501.sql
2. 修改admin模块中yml文件的数据源配置
3. 运行admin


#### 使用说明

1. 默认admin访问地址为127.0.0.1:8888/

