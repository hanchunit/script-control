package com.dawn.common.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * <p>script-control-com.dawn.config-MorningBreezeImageConfig</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Component
@ConfigurationProperties(prefix = "morningbreeze.image")
public class MorningBreezeImageConfig {
    /**
     * url路径地址
     */
    private static String srcPath;
    /**
     * 实际路径地址
     */
    private static String savaPath;
    /**
     * 文件最大字节数
     */
    private static Long maxFileSize;


    public static String getSrcPath() {
        return srcPath;
    }

    public void setSrcPath(String srcPath) {
        MorningBreezeImageConfig.srcPath = srcPath;
    }

    public static String getSavaPath() {
        return savaPath;
    }

    public void setSavaPath(String savaPath) {
        MorningBreezeImageConfig.savaPath = savaPath;
    }

    public static Long getMaxFileSize() {
        return maxFileSize;
    }

    public void setMaxFileSize(Long maxFileSize) {
        MorningBreezeImageConfig.maxFileSize = maxFileSize;
    }
}
