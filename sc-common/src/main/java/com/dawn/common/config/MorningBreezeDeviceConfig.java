package com.dawn.common.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * <p>script-control-com.dawn.common.config-MorningBreezeDeviceConfig</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


@Component
@ConfigurationProperties(prefix = "morningbreeze.device")
public class MorningBreezeDeviceConfig {
    /**
     * 最大设备数量
     */
    private static Long maximum;

    public static Long getMaximum() {
        return maximum;
    }

    public void setMaximum(Long maximum) {
        MorningBreezeDeviceConfig.maximum = maximum;
    }
}
