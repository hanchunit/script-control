package com.dawn.common.util.file.image;

import com.dawn.common.config.MorningBreezeImageConfig;
import com.dawn.common.exception.file.FileNotTypeException;
import com.dawn.common.exception.file.FileTypeException;
import com.dawn.common.exception.file.image.ImageNullException;
import com.dawn.common.exception.file.image.ImageSaveFailException;
import com.dawn.common.exception.file.image.ImageSizeOverrunException;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>script-control-com.dawn.common.util-ImageTool</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Component
public class ImageTool {

    /**
     * 文件类型
     */
    public final static Map<String, String> FILE_TYPE_MAP = new HashMap<>();
    /**
     * 文件根目录
     */
    private static final String SAVE_PATH = MorningBreezeImageConfig.getSavaPath();
    /**
     * 文件大小限制
     */
    private static final Long MAX_SIZE = MorningBreezeImageConfig.getMaxFileSize();

    static {
        getAllFileType();  //初始化文件类型信息
    }


    private static void getAllFileType() {
        FILE_TYPE_MAP.put("jpg", "FFD8FF");
        FILE_TYPE_MAP.put("png", "89504E47");
        FILE_TYPE_MAP.put("gif", "47494638");
        FILE_TYPE_MAP.put("bmp", "424D");
    }

    /**
     * 图片上传
     *
     * @param image     图片
     * @param modelName 目录名称
     * @param imageName 图片名称
     * @return 图片路径
     */
    public static String imageUpload(MultipartFile image, String modelName, String imageName) {
        //检查图片是否为空
        if (image == null || image.isEmpty()) {
            throw new ImageNullException();
        }

        //判断图片大小是否符合要求
        if (image.getSize() > MAX_SIZE) {
            throw new ImageSizeOverrunException();
        }

        // 文件原名称
        String fileName = image.getOriginalFilename();
        // 文件类型
        String type = null;
        //System.out.println("上传的文件原名称:" + fileName);
        // 判断文件类型
        type = fileName.contains(".") ? fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length()) : null;
        // 判断文件类型是否为空
        if (type != null) {

            if ("PNG".equals(type.toUpperCase()) || "JPG".equals(type.toUpperCase())) {

                // 项目在容器中实际发布运行的根路径
                //   String realPath = request.getSession().getServletContext().getRealPath("/");
                // 自定义的文件名称
                // 设置存放图片文件的路径
                String trueFileName = modelName + "\\" + imageName + "." + type;
                System.out.println("存放图片文件的路径:" + SAVE_PATH);
                File file1 = new File(SAVE_PATH);

                // 文件对象创建后，指定的文件或目录不一定物理上存在
                if (!file1.exists()) {
                    if (!file1.mkdir()) {
                        throw new ImageSaveFailException();
                    }
                }
                // 转存文件到指定的路径
                try {
                    image.transferTo(new File(SAVE_PATH+trueFileName));
                } catch (IOException e) {
                    throw new ImageSaveFailException();
                }
                System.out.println("文件成功上传到指定目录下");
                return trueFileName;
            } else {
                System.out.println("不是我们想要的文件类型,请按要求重新上传");
                throw new FileTypeException();
            }
        } else {
            System.out.println("文件类型为空");
            throw new FileNotTypeException();
        }
    }

    public MultipartFile imageDownload(String path) throws IOException {
        return null;
    }
}




