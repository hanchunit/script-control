package com.dawn.common.util;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Map;
import java.util.UUID;

/**
 * <p>script-control-com.dawn.common.util-TokenTool</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class TokenTool {


    public static String createToken(Map<String, Object> map) {

        map.put("uuid", UUID.randomUUID().toString().replace("-", ""));
        byte[] bytes = map.toString().getBytes(StandardCharsets.UTF_8);
        return Base64.getUrlEncoder().encodeToString(bytes);
    }

    public static String createToken() {
        byte[] bytes = UUID.randomUUID().toString().replace("-", "").getBytes(StandardCharsets.UTF_8);
        return Base64.getUrlEncoder().encodeToString(bytes);
    }

    /**
     * 解码
     *
     * @param token token
     * @return 数据
     */
    public static String decodeToken(String token) {
        return new String(Base64.getUrlDecoder().decode(token));
    }


}
