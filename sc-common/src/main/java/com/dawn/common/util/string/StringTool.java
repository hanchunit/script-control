package com.dawn.common.util.string;

/**
 * <p>script-control-com.dawn.common.util.string-StringTool</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class StringTool {
    /**
     * 判断字符串是否为空
     *
     * @param string 字符串
     * @return true|false
     */
    public static Boolean isNull(String string) {
        return string == null || string.isEmpty();
    }


}
