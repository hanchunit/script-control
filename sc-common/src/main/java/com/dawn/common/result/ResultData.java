package com.dawn.common.result;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * <p>script-control-com.dawn.common-ResultData</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Data
public class ResultData implements Serializable {
    private static final long serialVersionUID = -4928094880503110845L;

    @JsonValue
    private List<?> data;


    public ResultData(List<?> list) {
        this.data = list;
    }
}
