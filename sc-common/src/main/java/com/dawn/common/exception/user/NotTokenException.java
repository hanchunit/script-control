package com.dawn.common.exception.user;

import static com.dawn.common.constant.user.UserExceptionConstant.NOT_TOKEN_EXCEPTION;

/**
 * <p>script-control-com.dawn.common.exception.user.user-NotTokenException</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class NotTokenException extends UserException {
    private static final long serialVersionUID = 185112223367347338L;

    /**
     * 用户已在别处登录
     */
    public NotTokenException() {
        super(400, NOT_TOKEN_EXCEPTION, 1);
    }
}
