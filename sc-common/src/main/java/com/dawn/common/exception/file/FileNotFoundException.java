package com.dawn.common.exception.file;

/**
 * <p>script-control-com.dawn.common.exception.file-FileNotFoundException</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class FileNotFoundException extends FileException {
    private static final long serialVersionUID = -7107106824055861715L;

    /**
     * 文件为空
     */
    public FileNotFoundException() {
        super(400, "未找到文件，请检查图片路径");
    }
}
