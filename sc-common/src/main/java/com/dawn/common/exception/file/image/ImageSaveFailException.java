package com.dawn.common.exception.file.image;

/**
 * <p>script-control-com.dawn.common.exception.file-FileNotFoundException</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class ImageSaveFailException extends ImageException {

    private static final long serialVersionUID = -5665499104535130982L;

    /**
     * 图片保存失败
     */
    public ImageSaveFailException() {
        super(400, "图片保存失败！！！");
    }
}
