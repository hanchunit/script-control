package com.dawn.common.exception.user;

import com.dawn.common.constant.user.UserExceptionConstant;

/**
 * <p>script-control-com.dawn.common.exception.user-UserStateException</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class UserLockException extends UserException {

    private static final long serialVersionUID = -5048419817416410188L;

    /**
     * 用户已锁定
     */
    public UserLockException() {
        super(400, UserExceptionConstant.USER_LOCK_EXCEPTION);
    }
}
