package com.dawn.common.exception.user;

import static com.dawn.common.constant.user.UserExceptionConstant.NOT_LOGIN_OR_NOT_ACTIVATE_EXCEPTION;

/**
 * <p>script-control-com.dawn.common.exception.user.user-UserLoginException</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class NotLoginOrNotActivateException extends UserException {


    private static final long serialVersionUID = -8778297857397870781L;

    /**
     * 用户未登录或设备未激活异常
     */
    public NotLoginOrNotActivateException() {
        super(400, NOT_LOGIN_OR_NOT_ACTIVATE_EXCEPTION);
    }
}
