package com.dawn.common.exception.user.device;

import com.dawn.common.exception.base.BaseException;

/**
 * <p>script-control-com.dawn.common.exception.user.device-DeviceException</p>
 * <p>Description: 设备异常 </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class DeviceException extends BaseException {
    private static final long serialVersionUID = 1474445696174593552L;

    public DeviceException(Integer status, String msg) {
        super(status, msg);
    }

    public DeviceException(Integer status, String msg, Integer exist) {
        super(status, msg, exist);
    }

    /**
     * 设备异常
     */
    public DeviceException() {
        super(400, "设备异常");
    }
}
