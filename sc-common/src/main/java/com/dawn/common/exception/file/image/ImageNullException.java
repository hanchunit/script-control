package com.dawn.common.exception.file.image;

/**
 * <p>script-control-com.dawn.common.exception.file.image-ImageNullException</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class ImageNullException extends ImageException {
    private static final long serialVersionUID = 4634843675200778398L;

    /**
     * 图片为空
     */
    public ImageNullException() {
        super(400, "图片为空");
    }
}
