package com.dawn.common.exception.system;

import com.dawn.common.constant.system.SystemExceptionConstant;
import com.dawn.common.exception.base.BaseException;

/**
 * <p>script-control-com.dawn.common.exception.system-SystemException</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class SystemException extends BaseException {
    private static final long serialVersionUID = 5681589987429186753L;

    /**
     * 系统异常
     */
    public SystemException() {
        super(400, SystemExceptionConstant.SYSTEM_EXCEPTION);
    }
}
