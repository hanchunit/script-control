package com.dawn.common.exception.file.image;

/**
 * <p>script-control-com.dawn.common.exception.file-FileSizeOverrunException</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class ImageSizeOverrunException extends ImageException {


    private static final long serialVersionUID = -5428606055609530807L;

    /**
     * 图片过大
     */
    public ImageSizeOverrunException() {
        super(400, "图片大小超过限制，请重新上传");
    }
}
