package com.dawn.common.exception.user.device;

import static com.dawn.common.constant.user.device.DeviceExceptionConstant.NOT_ACTIVATE_EXCEPTION;

/**
 * <p>script-control-com.dawn.common.exception.user.device-NotActivateException</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class NotActivateException extends DeviceException {
    private static final long serialVersionUID = 7153782703059404874L;

    /**
     * 设备未激活
     */
    public NotActivateException() {
        super(400, NOT_ACTIVATE_EXCEPTION);
    }
}
