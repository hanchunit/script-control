package com.dawn.common.exception.user;

import static com.dawn.common.constant.user.UserExceptionConstant.NOT_USERNAME_EXCEPTION;

/**
 * <p>script-control-com.dawn.common.exception.user-NotUserException</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class NotUsernameException extends UserException {
    private static final long serialVersionUID = 600410741769547396L;

    /**
     * 用户名不存在
     */
    public NotUsernameException() {
        super(400, NOT_USERNAME_EXCEPTION, 0);
    }
}
