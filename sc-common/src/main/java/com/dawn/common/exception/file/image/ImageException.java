package com.dawn.common.exception.file.image;

import com.dawn.common.exception.file.FileException;

/**
 * <p>script-control-com.dawn.common.exception.file.image-ImageException</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class ImageException extends FileException {
    private static final long serialVersionUID = -2786768187373134770L;

    /**
     * 图片异常
     *
     * @param status 状态码
     * @param msg    异常信息
     */
    public ImageException(Integer status, String msg) {
        super(status, msg);
    }

    /**
     * 图片异常
     *
     * @param status 状态码
     * @param msg    异常信息
     * @param exist  是否存在
     */
    public ImageException(Integer status, String msg, Integer exist) {
        super(status, msg, exist);
    }
}
