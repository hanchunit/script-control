package com.dawn.common.exception.user;

import static com.dawn.common.constant.user.UserExceptionConstant.NOT_PERMISSION_EXCEPTION;

/**
 * <p>script-control-com.dawn.common.exception.user-NotPermissionException</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class NotPermissionException extends UserException {
    private static final long serialVersionUID = -7961126429596372506L;

    /**
     * 用户权限不足
     */
    public NotPermissionException() {
        super(400, NOT_PERMISSION_EXCEPTION);
    }
}
