package com.dawn.common.exception.file;

import com.dawn.common.exception.base.BaseException;

/**
 * <p>script-control-com.dawn.common.exception.file-FileException</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class FileException extends BaseException {
    private static final long serialVersionUID = -4537348692431209613L;

    /**
     * 文件异常
     *
     * @param status 状态码
     * @param msg    异常信息
     */
    public FileException(Integer status, String msg) {
        super(status, msg);
    }

    /**
     * 文件异常
     *
     * @param status 返回状态码
     * @param msg    异常信息
     * @param exist  是否存在
     */
    public FileException(Integer status, String msg, Integer exist) {
        super(status, msg, exist);
    }
}
