package com.dawn.common.exception.user;

import static com.dawn.common.constant.user.UserExceptionConstant.NOT_LOGIN_EXCEPTION;

/**
 * <p>script-control-com.dawn.common.exception.user.user-UserLoginException</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class NotLoginException extends UserException {
    private static final long serialVersionUID = -1378874653565655166L;

    /**
     * 用户未登录异常
     */
    public NotLoginException() {
        super(400, NOT_LOGIN_EXCEPTION);
    }
}
