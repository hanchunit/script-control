package com.dawn.common.exception.user.device;

import com.dawn.common.constant.user.device.DeviceExceptionConstant;

/**
 * <p>script-control-com.dawn.common.exception.user.device-DeviceIMEIException</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class DeviceIMEIException extends DeviceException {


    private static final long serialVersionUID = -8454130320393307154L;

    /**
     * 设备IMEI异常
     */
    public DeviceIMEIException() {
        super(400, DeviceExceptionConstant.DEVICE_IMEI_EXCEPTION);
    }
}
