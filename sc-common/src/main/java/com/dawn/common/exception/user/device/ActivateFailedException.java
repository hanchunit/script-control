package com.dawn.common.exception.user.device;

import com.dawn.common.constant.user.device.DeviceExceptionConstant;

/**
 * <p>script-control-com.dawn.common.exception.user.device-ActivateFailedException</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class ActivateFailedException extends DeviceException {
    private static final long serialVersionUID = 4747714215613080790L;

    /**
     * 设备激活失败异常
     */
    public ActivateFailedException() {
        super(400, DeviceExceptionConstant.ACTIVATE_FAILED_EXCEPTION);
    }
}
