package com.dawn.common.exception.user;

import com.dawn.common.constant.user.UserExceptionConstant;

/**
 * <p>script-control-com.dawn.common.exception.user-UserStateException</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class UserDisableException extends UserException {


    private static final long serialVersionUID = -360569160623842766L;

    /**
     * 用户已停用
     */
    public UserDisableException() {
        super(400, UserExceptionConstant.USER_DISABLE_EXCEPTION);
    }
}
