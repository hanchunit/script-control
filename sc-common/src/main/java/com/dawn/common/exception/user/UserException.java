package com.dawn.common.exception.user;


import com.dawn.common.exception.base.BaseException;

import static com.dawn.common.constant.user.UserExceptionConstant.USER_EXCEPTION;

/**
 * <p>script-control-com.dawn.common.exception.user-UserException</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

public class UserException extends BaseException {
    private static final long serialVersionUID = -6139062394178464772L;

    public UserException(Integer status, String msg, Integer exist) {
        super(status, msg, exist);
    }

    public UserException(Integer status, String msg) {
        super(status, msg);
    }

    /**
     * 用户异常
     */
    public UserException() {
        super(400, USER_EXCEPTION);
    }

}
