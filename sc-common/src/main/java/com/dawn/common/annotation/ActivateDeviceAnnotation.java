package com.dawn.common.annotation;

import org.springframework.context.annotation.EnableAspectJAutoProxy;

import java.lang.annotation.*;

/**
 * <p>script-control-com.dawn.common.annotation-ActivateDeviceAnnotation</p>
 * <p>Description: 验证设备激活状态注解 </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@EnableAspectJAutoProxy(proxyTargetClass = true)
public @interface ActivateDeviceAnnotation {
    /**
     * 验证激活
     *
     * @return 是否需要验证激活状态，默认为是
     */
    boolean isActivate() default true;
}
