package com.dawn.common.annotation;

import org.springframework.context.annotation.EnableAspectJAutoProxy;

import java.lang.annotation.*;

/**
 * <p>script-control-com.dawn.common.annotation-LoginAnnotation</p>
 * <p>Description: 登录验证注解 </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@EnableAspectJAutoProxy(proxyTargetClass = true)
public @interface LoginAnnotation {
    /**
     * 验证登录
     *
     * @return 是否需要验证登录，默认为是
     */
    boolean isLogin() default true;
}
