package com.dawn.common.annotation;

import org.springframework.context.annotation.EnableAspectJAutoProxy;

import java.lang.annotation.*;

/**
 * <p>script-control-com.dawn.common.annotation-TokenAnnotation</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@EnableAspectJAutoProxy(proxyTargetClass = true)
public @interface TokenAnnotation {
    /**
     * 权限名称
     *
     * @return 权限名称
     */
    String permissionName() default "";

    /**
     * 权限类型
     *
     * @return 权限类型
     */
    String permissionType() default "";

}
