package com.dawn.common.base;

import com.dawn.common.result.Result;
import com.dawn.common.result.ResultData;

import java.util.List;

/**
 * <p>script-control-com.dawn.common-BaseController</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

public class BaseController {
    protected Result result;

    protected static ResultData resultData(List<?> list) {

        return new ResultData(list);
    }


}
