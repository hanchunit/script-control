package com.dawn.common.base;

import java.io.Serializable;

/**
 * <p>script-control-com.dawn.common-BaseEntity</p>
 * <p>Description: 基础实体类 </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class BaseEntity implements Serializable {
    private static final long serialVersionUID = 3009085090725317468L;
}
