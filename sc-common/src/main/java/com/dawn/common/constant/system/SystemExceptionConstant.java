package com.dawn.common.constant.system;

/**
 * <p>script-control-com.dawn.common.constant.system.SystemExceptionConstant-SystemExceptionConstant</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class SystemExceptionConstant {

    public static String SYSTEM_EXCEPTION = "系统异常，请联系管理员";


}
