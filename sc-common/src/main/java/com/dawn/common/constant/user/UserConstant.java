package com.dawn.common.constant.user;

/**
 * <p>script-control-com.dawn.common.constant.user-UserConstant</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class UserConstant {
    /**
     * 用户状态:正常
     */
    public static int USER_STATE_NORMAL = 0;
    /**
     * 用户状态:锁定
     */
    public static int USER_STATE_LOCK = 1;
    /**
     * 用户状态:停用
     */
    public static int USER_STATE_DISABLE = 2;
    /**
     * 用户类型:超级管理员
     */
    public static int USER_TYPE_ROOT = 0;
    /**
     * 普通管理员
     */
    public static int USER_TYPE_ADMIN = 1;
    /**
     * 普通用户
     */
    public static int USER_TYPE_COMMON = 2;
}
