package com.dawn.common.constant.user.device;

/**
 * <p>script-control-com.dawn.common.constant.user.device-DeviceExceptionConstant</p>
 * <p>Description: 设备异常常量 </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class DeviceExceptionConstant {

    /**
     * 设备异常
     */
    public static String DEVICE_EXCEPTION = "设备异常";
    /**
     * 设备未激活异常
     */
    public static String NOT_ACTIVATE_EXCEPTION = "设备未激活，请先激活设备";
    /**
     * 设备激活失败
     */
    public static String ACTIVATE_FAILED_EXCEPTION = "设备激活失败，请重试";
    /**
     * IMEI异常
     */
    public static String DEVICE_IMEI_EXCEPTION = "设备IMEI异常，请检查后重试";
}
