package com.dawn.common.constant.user;

/**
 * <p>script-control-com.dawn.common.constant-UserExceptionConstant</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class UserExceptionConstant {
    /**
     * 用户异常
     */
    public static String USER_EXCEPTION = "用户异常";
    /**
     * 用户名不存在
     */
    public static String NOT_USERNAME_EXCEPTION = "用户名不存在";
    /**
     * 用户名或密码错误
     */
    public static String NOT_USERNAME_OR_PASSWORD_EXCEPTION = "用户名或密码错误";
    /**
     * 用户权限不足
     */
    public static String NOT_PERMISSION_EXCEPTION = "用户权限不足，不允许访问";
    /**
     * 账户已在别处登录
     */
    public static String NOT_TOKEN_EXCEPTION = "帐号已在别处登录，请重新登录";
    /**
     * 创建token失败
     */
    public static String SET_TOKEN_EXCEPTION = "创建Token失败，请联系管理员";
    /**
     * 账号未登录
     */
    public static String NOT_LOGIN_EXCEPTION = "账号未登录，请先登录帐号";
    /**
     * 用户已锁定
     */
    public static String USER_LOCK_EXCEPTION = "用户已被锁定，请联系管理员";
    /**
     * 用户已停用
     */
    public static String USER_DISABLE_EXCEPTION = "用户已停用，请联系管理员";
    /**
     * 用户状态异常
     */
    public static String USER_STATE_EXCEPTION = "用户状态异常，请联系管理员";

    /**
     * 账号未登录或设备未激活
     */
    public static String NOT_LOGIN_OR_NOT_ACTIVATE_EXCEPTION = "账号未登录或设备未激活，请先登录帐号或激活设备";
}
